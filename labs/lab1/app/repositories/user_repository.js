const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers() { 
        const items = this.storage.readItems();
        return items["items"];
        // throw new Error("Not implemented"); 
    }
 
    getUserById(id) {
        const items = this.storage.readItems();
        // console.log(typeof(items));
        for (const item of items["items"]) {
            if (item.id === id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }
};
 
module.exports = UserRepository;

