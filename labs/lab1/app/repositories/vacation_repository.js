

const Vacation = require('../models/vacation');
const JsonStorage = require('../jsonStorage');
const readlineSync = require('readline-sync');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
  }

class VacationRepository {
    
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    addVacation(vacation) {

        let jsonArray = this.storage.readItems();
        vacation.id = jsonArray["nextId"];
        let id = vacation.id;
        jsonArray["nextId"] += 1;
        jsonArray["items"].push(vacation);
        this.storage.writeChanges(jsonArray);
        return id;
        // throw new Error("Not implemented"); 
    }

    getVacations() {
        const items = this.storage.readItems();
        return items["items"];
        // throw new Error("Not implemented"); 
    }

    getVacationById(vacation_id) {
        const items = this.storage.readItems();
        // console.log(typeof(items));
        for (const item of items["items"]) {
            if (item.id == vacation_id) {
                return new Vacation(item.id, item.tour_operator, item.country,
                    item.duration, item.cost, item.date);
            }
        }
        return null;
        // throw new Error("Not implemented"); 
    }

    updateVacation (vacation) {
        const items = this.storage.readItems();
        for (let i = 0; i < items["items"].length; i++) {
            if (items["items"][i].id == vacation.id) {
                items["items"][i] = vacation;
                this.storage.writeChanges(items);
                return true;
            }
        }
        return false;
        // throw new Error("Not implemented"); 
    }

    deleteVacation (vacation_id) {
        const items = this.storage.readItems();
        for (let i = 0; i < items["items"].length; i++) {
            if (items["items"][i].id === vacation_id) {
                items["items"].splice(i, 1);
                this.storage.writeChanges(items);
                return true;
            }
        }
        return false;
    }

    createVacation() {
        let vac = new Vacation();
        vac.tour_operator = readlineSync.question("Enter tour operator name:").trim();
        while (true)
        {
            try {
                let cost = readlineSync.question("Enter cost:").trim()
                if (!isNumeric(cost))
                {
                    throw TypeError("Cost should be entered as positive integer. Try again.")
                }
                vac.cost = Number(cost)
            }
            catch (e) {
                if (e instanceof TypeError) {
                    console.log(e.message)
                    continue
                }
            }
            break
        }
        
        vac.country = readlineSync.question("Enter country: ").trim();
        
        while (true)
        {
            try {
                let dur = readlineSync.question("Enter duration: ").trim();
                if (!isNumeric(dur))
                {
                    throw TypeError("Duration should be entered as positive integer. Try again.")
                }
                vac.duration = Number(dur)
            }
            catch (e) {
                if (e instanceof TypeError) {
                    console.log(e.message)
                    continue
                }
            }
            break
        }
        let date
        while (true) {
            let year = readlineSync.question("Enter year: ").trim()
            let month = readlineSync.question("Enter month (number): ").trim()
            let day = readlineSync.question("Enter day: ").trim()
            let hours = readlineSync.question("Enter hours: ").trim()
            let minutes = readlineSync.question("Enter minutes: ").trim()
            date = new Date(year, month, day, hours, minutes)
            if (isValidDate(date))
            {
                break
            }
            console.log("Invalid date. Try again")
        }
        vac.date = date
        return vac;
    }

}

module.exports = VacationRepository;