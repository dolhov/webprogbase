

class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login; // string
        this.fullname = fullname;  // string
        this.role = role; // bool
        this.registeredAt = registeredAt; // date ISO 8601
        this.avaUrl = avaUrl; // string with url of avatar
        this.isEnabled = isEnabled; // bool, is user is activated (via email?)

        // TODO: More fields
    }
 };
 
module.exports = User;
 