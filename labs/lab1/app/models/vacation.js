

class Vacation {

    constructor (id, tour_operator, country, duration, cost, date) {
        this.id = id;
        this.tour_operator = tour_operator;
        this.country = country;
        this.duration = duration;
        this.cost = cost;
        this.date = date;
    }
}



module.exports = Vacation;