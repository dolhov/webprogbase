const userController = require('./controllers/user_controller');
const vacationController = require('./controllers/vacation_controller');
const Router = require('./routers/router.js');
 
const router = new Router();
router.use("get/users", userController.getUsers);
router.use("get/user", userController.getUser);
router.use("post/vacation", vacationController.postVacation);
router.use("get/vacation", vacationController.getVacation);
router.use("get/vacations", vacationController.getVacations);
router.use("delete/vacation", vacationController.deleteVacation);
router.use("update/vacation", vacationController.updateVacation);

const readlineSync = require('readline-sync');

while (true) {
    const data = readlineSync.question("Enter command:");
    const text = data.toString().trim();
    // example: "user/1"
    const parts = text.split("/");
    const method = parts[0];
    const command = parts[1];
    //
    const input = parts[2];
    const output = function (message) { 
        console.log(message); 
    };
    router.handle(method + "/" + command, input, output);
}

