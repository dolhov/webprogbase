

const UserRepository = require('./../repositories/user_repository');
 
const userRepository = new UserRepository("data/users.json");
 
module.exports = {
 
    getUsers(input, output) {
        const users = userRepository.getUsers();
        let us = [];
        for (i = 0; i < users.length; i++) {
            let dc = {}
            us += `\"id\": ${users[i]["id"]}, "fullname": ${users[i]["fullname"]}\n`;
        }
        output(us);
        // throw new Error("Not implemented");
    },
 
    getUser(input, output) {
        const userId = parseInt(input);
        const user = userRepository.getUserById(userId);
        if (!user) {
            output(`Error: user with id ${userId} not found.`);
            return;
        }
        let ans = "";
        for (key in user) {
            ans += key + ": " + user[key] + "\n"
        }
        output(ans);
    },
};
