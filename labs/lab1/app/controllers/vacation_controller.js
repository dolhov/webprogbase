const VacationRepository = require('./../repositories/vacation_repository');
const Vacation = require('../models/vacation');


const vacationRepository = new VacationRepository("data/vacations.json");
 
module.exports = {

    postVacation(input, output) {
        let vac = vacationRepository.createVacation();
        const new_id = vacationRepository.addVacation(vac);
        output(`Posted succesfully.Posted vacation id is: ${new_id}`)
    },

    getVacations(input, output) {
        const vacations = vacationRepository.getVacations();
        let text = ``;
        for (const item of vacations) {
            text += (`id: "${item.id}; Country: "${item.country}"\n`);
        }
        output(text);
        // throw new Error("Not implemented");
    },

    getVacation(input, output) {
        const vacation_id = parseInt(input);
        const vac = vacationRepository.getVacationById(vacation_id);
        if (!vac) {
            output(`Error: vacation with id ${vacation_id} not found.`);
            return;
        }
        let ans = ""
        for (key in vac) {
            ans += key + ": " + vac[key] + "\n"
        }
        output(ans);
    },

    updateVacation(input, output) {
        if (!input) {
            console.log("No id provided");
            return;
        }
        // if (!Number.isInteger(input)) {
        //     console.log("Invalid input");
        // }
        let vc = vacationRepository.getVacationById(Number(input))
        if (!vc) {
            output(`Error: vacation with id ${input} not found.`);
            return;
        }
        console.log("Leave field empty if you don't want to change it")
        let vacation = vacationRepository.createVacation();
        vacation.id = Number(input)
        if (!vacation.cost) 
        {
            vacation.cost = vc.cost
        }
        if (!vacation.country)
        {
            vacation.country = vc.country
        }
        if (!vacation.date)
        {
            vacation.date = vc.date
        }
        if (!vacation.duration)
        {
            vacation.duration = vc.duration
        }
        if (!vacation.tour_operator)
        {
            vacation.tour_operator = vc.tour_operator
        }
        const vac = vacationRepository.updateVacation(vacation);
        if (!vac) {
            output(`Error: vacation with id ${vacation.id} not found.`);
            return;
        }
        output("Updated succesfully")
    },


    deleteVacation(input, output) {
        const vacation_id = parseInt(input);
        if(!vacationRepository.deleteVacation(vacation_id)) {
            output(`Error: vacation with id ${vacation_id} not found.`);
            return;
        }
        output("Deleted succesfully")
    },
};
