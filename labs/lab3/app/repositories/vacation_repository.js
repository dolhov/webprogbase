

const Vacation = require('../models/vacation');
const JsonStorage = require('../jsonStorage');
const readlineSync = require('readline-sync');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
  }

class VacationRepository {
    
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }

    addVacation(vacation) {

        let jsonArray = this.storage.readItems();
        vacation.id = jsonArray["nextId"];
        let id = vacation.id;
        jsonArray["nextId"] += 1;
        jsonArray["items"].push(vacation);
        this.storage.writeChanges(jsonArray);
        return id;
        // throw new Error("Not implemented"); 
    }

    getVacations(lim, offset, search_name = "") {
        console.log(search_name)
        let limit = lim - offset
        const items = this.storage.readItems();
        let ans = []
        let i = offset
        console.log(limit, offset, search_name, items["items"].length)
        let j = 0
        while(true) {
            if (j < items["items"].length)
            {
                if (search_name) {
                    if (items["items"][j]["tour_operator"].startsWith(search_name)) {
                        if (i > 0) {
                            i -= 1
                        }
                        else if (ans.length < limit){
                            ans.push(items["items"][j])
                        }
                        else {
                            return ans
                        }
                    }
                }
                else {
                    if (i > 0) {
                        i -= 1
                    }
                    else if (ans.length < limit){
                        ans.push(items["items"][j])
                    }
                    else {
                        return ans
                    }
                }
            }
            else {
                break
            }  
            j += 1       
        }
        return ans;
        // throw new Error("Not implemented"); 
    }

    getVacationsLength(tourop_name="") {
        
        const items = this.storage.readItems();
        let counter = 0
        let i = 0
        while (i < items["items"].length) {

            if (tourop_name) {
                if (items["items"][i]["tour_operator"].startsWith(tourop_name)) {
                    counter += 1
                }
            }
            else {
                counter += 1
            }
            i++
        }
        console.log(counter)
        return counter
    }

    getVacationById(vacation_id) {
        const items = this.storage.readItems();
        // console.log(typeof(items));
        for (const item of items["items"]) {
            if (item.id == vacation_id) {
                return new Vacation(item.id, item.tour_operator, item.country,
                    item.duration, item.cost, item.date, item.image);
            }
        }
        return null;
        // throw new Error("Not implemented"); 
    }

    updateVacation (vacation) {
        const items = this.storage.readItems();
        for (let i = 0; i < items["items"].length; i++) {
            if (items["items"][i].id == vacation.id) {
                items["items"][i] = vacation;
                this.storage.writeChanges(items);
                return vacation;
            }
        }
        return None;
        // throw new Error("Not implemented"); 
    }

    deleteVacation (vacation_id) {
        let vc
        const items = this.storage.readItems();
        for (let i = 0; i < items["items"].length; i++) {
            if (items["items"][i].id === vacation_id) {
                vc = items["items"][i]
                items["items"].splice(i, 1);
                this.storage.writeChanges(items);
                return vc;
            }
        }
        return vc;
    }

    createVacation() {
        let vac = new Vacation();
        vac.tour_operator = readlineSync.question("Enter tour operator name:").trim();
        while (true)
        {
            try {
                let cost = readlineSync.question("Enter cost:").trim()
                if (!isNumeric(cost))
                {
                    throw TypeError("Cost should be entered as positive integer. Try again.")
                }
                vac.cost = Number(cost)
            }
            catch (e) {
                if (e instanceof TypeError) {
                    console.log(e.message)
                    continue
                }
            }
            break
        }
        
        vac.country = readlineSync.question("Enter country: ").trim();
        
        while (true)
        {
            try {
                let dur = readlineSync.question("Enter duration: ").trim();
                if (!isNumeric(dur))
                {
                    throw TypeError("Duration should be entered as positive integer. Try again.")
                }
                vac.duration = Number(dur)
            }
            catch (e) {
                if (e instanceof TypeError) {
                    console.log(e.message)
                    continue
                }
            }
            break
        }
        let date
        while (true) {
            let year = readlineSync.question("Enter year: ").trim()
            let month = readlineSync.question("Enter month (number): ").trim()
            let day = readlineSync.question("Enter day: ").trim()
            let hours = readlineSync.question("Enter hours: ").trim()
            let minutes = readlineSync.question("Enter minutes: ").trim()
            date = new Date(year, month, day, hours, minutes)
            if (isValidDate(date))
            {
                break
            }
            console.log("Invalid date. Try again")
        }
        vac.date = date
        return vac;
    }

    convertVacationToDict(vac) {
        return {'id': vac.id, 'tour_operator': vac.tour_operator, 'country': vac.country, 
                'duration': vac.duration, 'cost': vac.cost, 'date': vac.date, 'image': vac.image}
    }

}

module.exports = VacationRepository;