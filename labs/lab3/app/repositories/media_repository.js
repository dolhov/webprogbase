const fs = require('fs');

class mediaRepository {


    postMedia(media) {
        const str_id = fs.readFileSync("./data/media/id.txt");
        const id = parseInt(str_id)
        fs.writeFileSync("./data/media/id.txt", String(id + 1))
        fs.writeFileSync(`./data/media/${id}`, media)
        return id
    }

    getMedia(id) {
        try {
            let pic = fs.readFileSync(`./data/media/${id}`);
            return pic
        }
        catch {
            return
        }
    }
}

module.exports = mediaRepository