const fs = require('fs');
 
class JsonStorage {
 
    // filePath - path to JSON file
    constructor(filePath) {
        this.filePath = filePath;
    }
 
    readItems() {
        const jsonText = fs.readFileSync(this.filePath);
        const jsonArray = JSON.parse(jsonText);
        return jsonArray;
    }

    writeChanges(jsonArray) {
        fs.writeFileSync(this.filePath, JSON.stringify(jsonArray, null, 2));
    }
};
 
module.exports = JsonStorage;
