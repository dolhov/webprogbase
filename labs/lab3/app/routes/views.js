const express = require('express');
const html_controller = require("../controllers/html_controller")
const router = express.Router()

router.get("/vacations/new", html_controller.getNew)
router.post("/vacations/new", html_controller.postVacation)

router.post("/vacations/delete/:id", html_controller.deleteVacation)

router.get("/", html_controller.getIndex)
router.get("/users", html_controller.getUsers)
router.get("/users/:id", html_controller.getUser)
router.get("/vacations", html_controller.getVacations)
router.get("/vacations/:id", html_controller.getVacation)
router.get("/about", html_controller.getAbout)



module.exports = router;