const express = require('express');
const router = express.Router();
const userController = require('../controllers/user_controller.js');

/**
 * get all users (pagination possible)
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query - page number
 * @param {integer} limit.query - items per page
 * @returns {Array.<User>} 200 - all users
 */


router.get("/", userController.getUsers);

/**
 * get user by his id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {User.model} 200 - User object
 * @returns {integer} 404 - User not found
 * 
 */


router.get("/:id", userController.getUser);


module.exports = router;
