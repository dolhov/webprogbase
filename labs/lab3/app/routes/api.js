const router = require('express').Router();

const userRouter = require('./users');
const vactionRouter = require('./vacations')
const mediaRouter = require('./media')
const htmlRouter = require('./views')
const dataRouter = require('./data')

router.use('/users', userRouter);
router.use('/vacations', vactionRouter)
router.use('/media', mediaRouter)
router.use('/views', htmlRouter)
router.use('/data', dataRouter)

module.exports = router;
