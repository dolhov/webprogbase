const express = require('express');
const html_controller = require("../controllers/html_controller")
const router = express.Router()

router.get("/media/:id", html_controller.getImage)

module.exports = router;