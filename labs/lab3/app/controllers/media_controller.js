const mediaRepository = require('./../repositories/media_repository');
const media_repository = new mediaRepository();

// will work incorrectly due to changes in mediaRepository

module.exports = {

    getImage(req, res) {
        const imgId = req.params.id
        const pic = media_repository.getMedia(imgId)
        if (!pic) {
            res.status(404).send(`Not found`);
            return;
        }
        res.send(pic)
    },

    postImage(req, res) {
        let vac = req.files['image'].data
        let id = media_repository.postMedia(vac)
        res.send(`Created image with id: ${id}`)
    }
}