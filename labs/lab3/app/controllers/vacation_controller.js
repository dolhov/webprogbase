const VacationRepository = require('./../repositories/vacation_repository');
const Vacation = require('../models/vacation');



const vacationRepository = new VacationRepository("data/vacations.json");
 
module.exports = {

    postVacation(req, res) {
        let vac
        try {
            vac = JSON.parse(req.files['vacation'].data.toString())
            let tourop = vac["tour_operator"]
            let country = vac["country"]
            let duration = vac["duration"]
            let cost = vac["cost"]
            let date = vac["date"]
            if (!tourop || !country || !duration || !cost || !date) {
                res.status(400).send("Bad request")
            return
            }
        }
        catch {
            res.status(400).send("Bad request")
            return
        }
        const new_id = vacationRepository.addVacation(vac);
        res.send(vacationRepository.getVacationById(new_id))
    },

    getVacations(req, res) {
        let limit = parseInt(req.query.limit)
        let page = parseInt(req.query.page)
        if (page < 1) {
            res.status(400).send("Bad request")
        }
        const vacations = vacationRepository.getVacations(limit, (page - 1) * limit);
        if (vacations) {
            res.send(vacations);
        }
    },

    getVacation(req, res) {
        const vacation_id = parseInt(req.params.id);
        const vac = vacationRepository.getVacationById(vacation_id);
        if (!vac) {
            res.status(404).send(`Not found.`);
            return;
        }
        res.send(vac);
    },

    updateVacation(req, res) {
        let vacation
        try {
            vacation = JSON.parse(req.files['vacation'].data.toString())
            let id = vacation["id"]
            let tourop = vacation["tour_operator"]
            let country = vacation["country"]
            let duration = vacation["duration"]
            let cost = vacation["cost"]
            let date = vacation["date"]
            if (!tourop || !country || !duration || !cost || !date || !id) {
                res.status(400).send("Bad request")
                return
            }
        }
        catch {
            res.status(400).send("Bad request")
            return
        }
        const vac = vacationRepository.updateVacation(vacation);
        if (!vac) {
            res.status(404).send("Not found");
            return;
        }
        res.send(vac)
    },


    deleteVacation(req, res) {
        const vacation_id = parseInt(req.params.id);
        let vc = vacationRepository.deleteVacation(vacation_id)
        if (!vc)  {
            res.status(404).send("Not found")
        }
        res.send(vc)
    },
};
