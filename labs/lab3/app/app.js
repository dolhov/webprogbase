

// const Router = require('./routers/router.js');
const express = require('express');
const apiRouter = require('./routes/api');
let port = 3001;

 
// const router = new Router();
// router.use("get/users", userController.getUsers);
// router.use("get/user", userController.getUser);
// router.use("post/vacation", vacationController.postVacation);
// router.use("get/vacation", vacationController.getVacation);
// router.use("get/vacations", vacationController.getVacations);
// router.use("delete/vacation", vacationController.deleteVacation);
// router.use("update/vacation", vacationController.updateVacation);

const mustache = require('mustache-express');
const path = require('path');

const app = express();

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

const busboy = require('busboy-body-parser');
app.use(express.static('public'));


const options = {
   limit: '5mb',
   multi: false,
};
app.use(busboy(options));
app.use('/api', apiRouter);
const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
 
const optionsSwagger = {
    swaggerDefinition: {
        info: {
            description: 'Server for operating vacations',
            title: 'Vacation Server',
            version: '1.0.0',
        },
        host: `localhost:${port}`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(optionsSwagger);





  

// app.use(function(err, req, res, next) { 
//     console.log(`On error: ${err.message}`); 
// });

app.listen(port, function() { console.log('Server is ready'); });