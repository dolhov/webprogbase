from typing import Optional
from fastapi import FastAPI, Response, status, Header
# import pika
import bcrypt
from datetime import datetime
# from models.user import User
# from models.db_session import session
# from sqlalchemy.exc import SQLAlchemyError
# from sqlalchemy import select
import json
import jwt
from auth_db_handler import add_user, get_user
import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

SECRET_SALT = os.environ['SECRET_SALT']


app = FastAPI()
# RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters('localhost', heartbeat=0,
#                                                                         blocked_connection_timeout=300))
# EXCHANGE_CHANNEL = RABBITMQ_CONNECTION.channel()
# EXCHANGE_CHANNEL.exchange_declare(exchange='new_auth',
#                                   exchange_type='fanout')
# CHANNEL_SEND.basic_publish(exchange='test', routing_key='test',
#                       body=b'Test message.')


@app.post('/register', response_model=str, responses={400: {"model": str}, 409: {"model": str}}, status_code=200)
def register_user(response: Response, login: str = Header(None), password: str = Header(None)) -> int:
    if not (login and password):
        response.status_code = status.HTTP_400_BAD_REQUEST
        return 'No login or password provided'
    hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
    registered_at = datetime.now()
    try:
        usr_id = add_user(login=login, password=hashed_password.decode('utf-8'), registeredAt=registered_at.isoformat())
        return usr_id
    except ValueError as e:
        print(e)
        response.status_code = status.HTTP_409_CONFLICT
        return str(e)


@app.post("/login", response_model=str, responses={400: {"model": str}, 401: {"model": str}})
def login_user(response: Response, login: str = Header(None), password: str = Header(None)) -> str:
    if not (login and password):
        response.status_code = status.HTTP_400_BAD_REQUEST
        return 'No login or password provided'
    usr = get_user(login)
    if not usr:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return 'Non-existing login'
    if not bcrypt.checkpw(password.encode('utf-8'), usr.password.encode('utf-8')):
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return 'Wrong password'
    encoded_jwt = jwt.encode({'id': usr.id, "login": usr.login}, SECRET_SALT, algorithm="HS256")
    # TODO Добавить время истекания токена
    return encoded_jwt


@app.get('/usernameExists')
def is_login_exists(login: str) -> bool:
    pass
    # TODO Доделать
    # select_query = select(User).where(User.login == login)
    # try:
    #     usr = session.execute(select_query).fetchone()[0]
    #     return True
    # except TypeError:
    #     return False


@app.get('/users')
def get_all_users():
    pass

