from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session
import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

os.environ['AUTH_DB_URL']
POSTGRESQL_DATABASE_URL = os.environ['AUTH_DB_URL']


engine = create_engine(POSTGRESQL_DATABASE_URL, echo=False)
session = Session(bind=engine)

