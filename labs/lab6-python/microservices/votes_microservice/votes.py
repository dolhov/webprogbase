from typing import Optional
from fastapi import FastAPI, Response, status, Header
from pydantic import BaseModel
from datetime import date
# from models.vote import Vote
# from models.db_session import session
from sqlalchemy.exc import SQLAlchemyError
# from sqlalchemy import select
# from sqlalchemy import and_
# import pika
import jwt
import json
from datetime import datetime
from votes_db_handler import create_vote_db, create_vote_db

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

SECRET_SALT = os.environ['SECRET_SALT']

class Vote_pydantic(BaseModel):
    type_of_resource: str
    id_of_resource: int


app = FastAPI()


@app.post("/vote")
def create_vote(vote: Vote_pydantic, response: Response, encoded_jwt: str = Header(None)):
    try:
        id_login_dict = verify_jwt(encoded_jwt)
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)
    vote_id = create_vote_db(voted_by=id_login_dict['login'], id_of_resource=vote.id_of_resource,
         type_of_resource=vote.type_of_resource, time_of_vote=datetime.now())['id']
    if not vote_id:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return 'Database error'
    return vote_id


@app.post("/unvote")
def delete_vote (vote: Vote_pydantic, response: Response, encoded_jwt: str = Header(None)):
    try:
        id_login_dict = verify_jwt(encoded_jwt)
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)
    # select_query = select(Vote).where(and_(Vote.id == vote.id_of_resource,
    #                                        Vote.voted_by == id_login_dict['login']))
    try:
        vote_db = delete_vote_db(vote.id_of_resource, id_login_dict['login'])
    except SQLAlchemyError:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return 'Database error'
    if not vote_db:
        response.status_code = status.HTTP_404_NOT_FOUND
        return


def verify_jwt(encoded_jwt):
    try:
        id_login_dict = jwt.decode(encoded_jwt, SECRET_SALT, algorithms=["HS256"])
    except jwt.ExpiredSignatureError as e:
        raise ValueError('JWT token expired')
    except jwt.DecodeError as e:
        raise ValueError('Invalid JWT token')
    return id_login_dict
