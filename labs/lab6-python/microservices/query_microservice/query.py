import sys
sys.path.append('/home/axel/Desktop/uni/webprogbase/labs/lab6-python/microservices/query_microservice/')


import pika
from sqlalchemy.exc import SQLAlchemyError
from models_query.models import User
from models_query.models import Vacation
from models_query.models import Vote
from models_query.db_session import session
from sqlalchemy import select
from pydantic_response import VacationWithVotes, VoteWithUser, PydanticUser
from pydantic import PositiveInt
import json
import threading
from typing import Optional
import jwt
import sys
from query_db_handler import get_votes_users_vacations, get_vote_user_vacation, get_vacations_user_voted_for
from fastapi import FastAPI, Response, status, Header

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

SECRET_SALT = os.environ['SECRET_SALT']
RABBIT_CONNECTION_URL = os.environ['RABBITMQ_URL']


def auth_callback(ch, method, properties, body):
    print('Auth callback worked')
    usr = User(**json.loads(body))
    try:
        session.add(usr)
        session.commit()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()


def vacation_callback(ch, method, properties, body):
    print('Vacation callback worked')
    router = {'post': create_vacation, 'put': update_vacation, 'delete': delete_vacation}
    given_parameters = json.loads(body)
    method = given_parameters.pop('method', None)
    if not method:
        return
    router[method](**given_parameters)


def create_vacation(id, country, created_by, duration, cost, date_of_departure):
    vac = Vacation(id=id, created_by=created_by, country=country, duration=duration, cost=cost,
             date_of_departure=date_of_departure)
    try:
        session.add(vac)
        session.commit()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()


def update_vacation(id, country, created_by, duration, cost, date_of_departure):
    select_query = select(Vacation).where(Vacation.id == id)
    vacation = session.execute(select_query).fetchone()
    if not vacation:
        return
    vacation = vacation[0]
    vacation.id = id
    vacation.country = country
    vacation.created_by = created_by
    vacation.duration = duration
    vacation.cost = cost
    vacation.date_of_departure = date_of_departure
    try:
        session.commit()
    except SQLAlchemyError as e:
        print(e)
        return


def delete_vacation(id, country = '', created_by = '', duration = 0, cost = 0, date_of_departure = ''):
    select_query = select(Vacation).where(Vacation.id == id)
    vacation = session.execute(select_query).fetchone()
    if not vacation:
        return
    vacation = vacation[0]
    session.delete(vacation)
    try:
        session.commit()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()


def vacation_thread():
    print('thread started')
    RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL))
    CHANNEL_CONSUME_VACATIONS = RABBITMQ_CONNECTION.channel()
    CHANNEL_CONSUME_VACATIONS.exchange_declare('vacations', exchange_type='fanout')
    vacation_consumer = CHANNEL_CONSUME_VACATIONS.queue_declare(queue='consume_vacations')
    CHANNEL_CONSUME_VACATIONS.queue_bind(exchange='vacations',
                                         queue='consume_vacations')
    CHANNEL_CONSUME_VACATIONS.basic_consume(queue='consume_vacations', auto_ack=True,
                                            on_message_callback=vacation_callback)
    print(' [*] Waiting for messages for vacations consume. To exit press CTRL+C')
    CHANNEL_CONSUME_VACATIONS.start_consuming()

def votes_thread():
    print('votes thread started')
    RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL))
    CHANNEL_CONSUME_VOTES = RABBITMQ_CONNECTION.channel()
    CHANNEL_CONSUME_VOTES.exchange_declare('votes', exchange_type='fanout')
    vacation_consumer = CHANNEL_CONSUME_VOTES.queue_declare(queue='consume_votes')
    CHANNEL_CONSUME_VOTES.queue_bind(exchange='votes',
                                     queue='consume_votes')
    CHANNEL_CONSUME_VOTES.basic_consume(queue='consume_votes', auto_ack=True,
                                        on_message_callback=vote_callback)
    print(' [*] Waiting for messages for votes consume. To exit press CTRL+C')
    CHANNEL_CONSUME_VOTES.start_consuming()

def vote_callback(ch, method, properties, body):
    print('Vote callback worked')
    router = {'vote': post_vote, 'unvote': unvote}
    given_parameters = json.loads(body)
    method = given_parameters.pop('method', None)
    if not method:
        return
    router[method](**given_parameters)

def post_vote(id, voted_by, id_of_resource, time_of_vote, type_of_resource):
    vote_for_db = Vote(id=id, voted_by=voted_by, id_of_resource=id_of_resource,
                       type_of_resource=type_of_resource, time_of_vote=time_of_vote)
    session.add(vote_for_db)
    try:
        session.commit()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()

def unvote(id, voted_by, id_of_resource, time_of_vote, type_of_resource):
    select_query = select(Vote).where(Vote.id == id)
    vote = session.execute(select_query).fetchone()
    if not vote:
        return
    vote = vote[0]
    session.delete(vote)
    try:
        session.commit()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()

def auth_thread():
    RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL))
    CHANNEL_CONSUME = RABBITMQ_CONNECTION.channel()
    CHANNEL_CONSUME.exchange_declare('new_auth', exchange_type='fanout')
    result = CHANNEL_CONSUME.queue_declare(queue='consume_auth')
    queue_name = result.method.queue
    CHANNEL_CONSUME.queue_bind(exchange='new_auth',
                                queue=queue_name)
    print(queue_name)
    CHANNEL_CONSUME.basic_consume(queue=queue_name, auto_ack=True,
                                  on_message_callback=auth_callback)
    print(' [*] Waiting for messages for auth. To exit press CTRL+C')
    CHANNEL_CONSUME.start_consuming()


x = threading.Thread(target=vacation_thread)
x.daemon = True
x.start()


y = threading.Thread(target=votes_thread)
y.daemon = True
y.start()

z = threading.Thread(target=auth_thread)
z.daemon = True
z.start()

app = FastAPI()




@app.on_event('shutdown')
def shutdown():
    sys.exit()


@app.get('/vacations/votes', response_model=list[VacationWithVotes])
def get_vacations(response: Response, offset: int = 0, limit: Optional[PositiveInt] = 10):
    if offset < 0:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return
    res = get_votes_users_vacations(offset, limit)
    votes_for_vacations = {}
    for i in res:
        vac = VacationWithVotes()
        vot = VoteWithUser()
        usr = PydanticUser()
        # i == [(Vote, User, Vacation)]
        usr.id = i[1].id
        usr.login = i[1].login
        vot.time = i[0].time_of_vote
        vot.user = usr
        vac.vacation_id = i[2].id
        vac.country = i[2].country
        if i[2].id not in votes_for_vacations:
            vac.votes = []
            votes_for_vacations[i[2].id] = vac
            votes_for_vacations[i[2].id].votes.append(vot)
        else:
            votes_for_vacations[i[2].id].votes.append(vot)
    final_result = []
    for i in votes_for_vacations:
        final_result.append(votes_for_vacations[i])
    return final_result
    #     vac.votes.append(vot)
    #     if not vac.id in is_vacation_in_res:
    #         is_vacation_in_res.add(vac.id)
    #         res.append(vac)
    # for i in res:




@app.get('/vacations/{vacation_id}/votes', response_model=VacationWithVotes)
def get_vacation_by_id(vacation_id, response: Response):
    res = get_vote_user_vacation(vacation_id)
    votes_for_vacations = {}
    if not res:
        response.status_code = status.HTTP_404_NOT_FOUND
        return
    for i in res:
        vac = VacationWithVotes()
        vot = VoteWithUser()
        usr = PydanticUser()
        # i == [(Vote, User, Vacation)]
        usr.id = i[1].id
        usr.login = i[1].login
        vot.time = i[0].time_of_vote
        vot.user = usr
        vac.vacation_id = i[2].id
        vac.country = i[2].country
        if i[2].id not in votes_for_vacations:
            vac.votes = []
            votes_for_vacations[i[2].id] = vac
            votes_for_vacations[i[2].id].votes.append(vot)
        else:
            votes_for_vacations[i[2].id].votes.append(vot)
    final_result = []

    for i in votes_for_vacations:
        final_result.append(votes_for_vacations[i])
    return final_result[0]


@app.get('/user/{user_id}/votes', response_model=list[VacationWithVotes])
def get_vacation_user_voted(user_id, response: Response, offset: int = 0, limit: PositiveInt = 10):
    if offset < 0:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return 'Offset can\'t be a negative number'
    res = get_vacations_user_voted_for(user_id, offset, limit)
    votes_for_vacations = {}
    if not res:
        response.status_code = status.HTTP_404_NOT_FOUND
        return
    for i in res:
        vac = VacationWithVotes()
        vot = VoteWithUser()
        usr = PydanticUser()
        # i == [(Vote, User, Vacation)]
        usr.id = i[1].id
        usr.login = i[1].login
        vot.time = i[0].time_of_vote
        vot.user = usr
        vac.vacation_id = i[2].id
        vac.country = i[2].country
        if i[2].id not in votes_for_vacations:
            vac.votes = []
            votes_for_vacations[i[2].id] = vac
            votes_for_vacations[i[2].id].votes.append(vot)
        else:
            votes_for_vacations[i[2].id].votes.append(vot)
    final_result = []

    for i in votes_for_vacations:
        final_result.append(votes_for_vacations[i])
    return final_result

@app.get('/me/votes', response_model=list[VacationWithVotes])
def get_vacation_user_voted(response: Response, offset: int = 0, limit: PositiveInt = 10,
                            encoded_jwt: str = Header(None)):
    if offset < 0:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return 'Offset can\'t be a negative number'
    try:
        id_login_dict = verify_jwt(encoded_jwt)
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)
    res = get_vacations_user_voted_for(id_login_dict['id'], offset, limit)
    votes_for_vacations = {}
    if not res:
        response.status_code = status.HTTP_404_NOT_FOUND
        return
    for i in res:
        vac = VacationWithVotes()
        vot = VoteWithUser()
        usr = PydanticUser()
        # i == [(Vote, User, Vacation)]
        usr.id = i[1].id
        usr.login = i[1].login
        vot.time = i[0].time_of_vote
        vot.user = usr
        vac.vacation_id = i[2].id
        vac.country = i[2].country
        if i[2].id not in votes_for_vacations:
            vac.votes = []
            votes_for_vacations[i[2].id] = vac
            votes_for_vacations[i[2].id].votes.append(vot)
        else:
            votes_for_vacations[i[2].id].votes.append(vot)
    final_result = []

    for i in votes_for_vacations:
        final_result.append(votes_for_vacations[i])
    return final_result


def verify_jwt(encoded_jwt):
    try:
        id_login_dict = jwt.decode(encoded_jwt, SECRET_SALT, algorithms=["HS256"])
    except jwt.ExpiredSignatureError as e:
        raise ValueError('JWT token expired')
    except jwt.DecodeError as e:
        raise ValueError('Invalid JWT token')
    return id_login_dict

