from typing import Optional
from pydantic import BaseModel, Field, PositiveInt
from datetime import datetime


class PydanticUser(BaseModel):
    id: Optional[PositiveInt]
    login: Optional[str]


class VoteWithUser(BaseModel):
    time: Optional[datetime]
    user: Optional[PydanticUser]


class VacationWithVotes(BaseModel):
    vacation_id: Optional[PositiveInt]
    country: Optional[str]
    votes: Optional[list[VoteWithUser]]

