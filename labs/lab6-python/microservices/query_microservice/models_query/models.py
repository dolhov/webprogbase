from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date, DateTime, ForeignKey
from .db_session import engine

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    login = Column(String, unique=True)
    password = Column(String)
    registeredAt = Column(DateTime)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Vacation(Base):
    __tablename__ = 'vacations'

    id = Column(Integer, primary_key=True)
    created_by = Column(String)
    country = Column(String)
    duration = Column(Integer)
    cost = Column(Integer)
    date_of_departure = Column(Date)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

class Vote(Base):
    __tablename__ = 'votes'

    id = Column(Integer, primary_key=True)
    voted_by = Column(String, ForeignKey('users.login'))
    id_of_resource = Column(Integer, ForeignKey('vacations.id'))
    time_of_vote = Column(DateTime)
    type_of_resource = Column(String)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


if __name__ == '__main__':
    # from sqlalchemy.orm import sessionmaker

    # USER = Config.USER
    # PASSWORD = Config.PASSWORD
    # DB = Config.DB
    # HOST = Config.HOST

    # engine = create_engine(POSTRESQL_DATABASE_URL, echo=True)
    # session = sessionmaker()
    # session.configure(bind=engine)
    Base.metadata.create_all(engine)