from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Date
from .db_session import engine

Base = declarative_base()


class Vacation(Base):
    __tablename__ = 'vacations'

    id = Column(Integer, primary_key=True)
    created_by = Column(String)
    country = Column(String)
    duration = Column(Integer)
    cost = Column(Integer)
    date_of_departure = Column(Date)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


if __name__ == '__main__':
    # from sqlalchemy.orm import sessionmaker

    # USER = Config.USER
    # PASSWORD = Config.PASSWORD
    # DB = Config.DB
    # HOST = Config.HOST

    # engine = create_engine(POSTRESQL_DATABASE_URL, echo=True)
    # session = sessionmaker()
    # session.configure(bind=engine)
    Base.metadata.create_all(engine)