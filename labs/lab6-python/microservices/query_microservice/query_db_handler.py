import sys
sys.path.append('/home/axel/Desktop/uni/webprogbase/labs/lab6-python/microservices/query_microservice/')

from models_query.models import User
from models_query.models import Vacation
from models_query.models import Vote
from models_query.db_session import session
from sqlalchemy import select


def get_votes_users_vacations(offset = 0, limit = 10):
    """

    :param offset:
    :param limit:
    :return: [(Vote, User, Vacation)]
    Returns list of tuples with vote, user, who voted, and the vacation, which was voted
    """
    query = select(Vote, User, Vacation).join(User).join(Vacation). \
        order_by(Vote.time_of_vote.desc()).offset(offset).limit(limit)
    res = session.execute(query).all()
    return res


def get_vote_user_vacation(vacation_id):
    query = select(Vote, User, Vacation).join(User).join(Vacation). \
        where(Vacation.id == vacation_id)
    res = session.execute(query).all()
    return res


def get_vacations_user_voted_for(user_id, offset = 0, limit = 10):
    query = select(Vote, User, Vacation).join(User).join(Vacation). \
        where(User.id == user_id).order_by(Vote.time_of_vote.desc()).offset(offset).limit(limit)
    res = session.execute(query).all()
    return res