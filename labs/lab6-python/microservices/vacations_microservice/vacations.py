from typing import Optional
from fastapi import FastAPI, Response, status, Header
from datetime import date
# from models.vacation import Vacation
# from models.db_session import session
from sqlalchemy.exc import SQLAlchemyError
# from sqlalchemy import select
import pika
import jwt
import json
from vacations_db_handler import create_vacation_db, update_vacation_db, delete_vacation_db, get_vacations_db, get_vacation_by_id_db

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

SECRET_SALT = os.environ['SECRET_SALT']
RABBIT_CONNECTION_URL = os.environ['RABBITMQ_URL']

RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL, heartbeat=0,
                                                                        blocked_connection_timeout=300))

EXCHANGE_CHANNEL = RABBITMQ_CONNECTION.channel()
EXCHANGE_CHANNEL.exchange_declare(exchange='vacations',
                                  exchange_type='fanout')

app = FastAPI()


@app.post('/vacations')
def create_vacation(country: str, duration: int, cost: int, date_of_departure: date,
                    response: Response, encoded_jwt: str = Header(None)):
    if not encoded_jwt:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return 'No jwt given'
    try:
        id_login_dict = verify_jwt(encoded_jwt)
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)
    if duration <= 0 or cost <= 0:
        response.status_code = status.HTTP_400_BAD_REQUEST
        return 'Duration/cost can\'t be less then 0'
    try:
        vac_id = create_vacation_db(created_by=id_login_dict['login'], country=country, duration=duration, cost=cost, date_of_departure=date_of_departure)
    except SQLAlchemyError as e:

        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return 'Database error'
    return vac_id

@app.put('/vacations/{vacation_id}')
def update_vacation(vacation_id, response: Response, encoded_jwt: str = Header(None),
                    country: Optional[str] = '', duration: Optional[int] = 0,
                    cost: Optional[int] = 0, date_of_departure: Optional[date] = ''):
    if not encoded_jwt:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return 'No jwt given'
    try:
        id_login_dict = verify_jwt(encoded_jwt)
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)
    try:
        result = update_vacation_db(vacation_id, id_login_dict['login'], country, duration, cost, date_of_departure)
    except SQLAlchemyError:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return 'Database error'
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return 'Only owner can delete a vacation'
    if not result:
        response.status_code = status.HTTP_404_NOT_FOUND
        return
    return result



@app.delete('/vacations/{vacation_id}')
def delete_vacation(vacation_id, response: Response, encoded_jwt: str = Header(None)):
    try:
        id_login_dict = verify_jwt(encoded_jwt)
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)

    try:
        vacation = delete_vacation_db(vacation_id, id_login_dict['login'])
    except ValueError as e:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return str(e)
    except SQLAlchemyError as e:
        response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        return 'Database error'
    if not vacation:
        response.status_code = status.HTTP_404_NOT_FOUND
        return
    return True

@app.get('/vacations')
def get_vacations(response: Response):
    # TODO Передалать с пагинацией
    return get_vacations_db()

@app.get('/vacations/{vacation_id}')
def get_vacation_by_id(vacation_id, response: Response):
    vacation = get_vacation_by_id_db(vacation_id)
    if not vacation:
        response.status_code = status.HTTP_404_NOT_FOUND
        return
    return vacation

def verify_jwt(encoded_jwt):
    try:
        id_login_dict = jwt.decode(encoded_jwt, SECRET_SALT, algorithms=["HS256"])
    except jwt.ExpiredSignatureError as e:
        raise ValueError('JWT token expired')
    except jwt.DecodeError as e:
        raise ValueError('Invalid JWT token')
    return id_login_dict