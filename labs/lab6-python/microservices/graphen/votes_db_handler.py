import sys
sys.path.append('/home/axel/Desktop/uni/webprogbase/labs/lab6-python/microservices/votes_microservice/models')

from models_vote.vote import Vote
from models_vote.db_session_votes import session
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import select
from sqlalchemy import and_
import pika
import json

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

RABBIT_CONNECTION_URL = os.environ['RABBITMQ_URL']

RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL, heartbeat=0,
                                                                        blocked_connection_timeout=300))

EXCHANGE_CHANNEL = RABBITMQ_CONNECTION.channel()
EXCHANGE_CHANNEL.exchange_declare(exchange='votes',
                                  exchange_type='fanout')


def create_vote_db(voted_by, id_of_resource, type_of_resource, time_of_vote):
    vote_for_db = Vote(voted_by=voted_by, id_of_resource=id_of_resource,
                       type_of_resource=type_of_resource, time_of_vote=time_of_vote)
    session.add(vote_for_db)
    try:
        session.commit()
        body = vote_for_db.as_dict()
        body['method'] = 'vote'
        EXCHANGE_CHANNEL.basic_publish('votes', routing_key='', body=json.dumps(body, default=str))
    except SQLAlchemyError as e:
        print(e)
        session.rollback()
        return
    return vote_for_db.as_dict()


def delete_vote_db(vote_id, login_of_user_voted):
    select_query = select(Vote).where(and_(Vote.id == vote_id,
                                           Vote.voted_by == login_of_user_voted))
    vote_db = session.execute(select_query).fetchone()
    if not vote_db:
        return
    vote_db = vote_db[0]
    session.delete(vote_db)
    try:
        session.commit()
        body = vote_db.as_dict()
        body['method'] = 'unvote'
        EXCHANGE_CHANNEL.basic_publish('votes', routing_key='', body=json.dumps(body, default=str))
        return vote_db.as_dict()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()
        raise e
        # response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        # return 'Database error'