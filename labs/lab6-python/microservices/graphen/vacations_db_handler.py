import sys
sys.path.append('/home/axel/Desktop/uni/webprogbase/labs/lab6-python/microservices/vacations_microservice/models')

from models_vacation.vacation import Vacation
from models_vacation.db_session import session
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import select
import pika
import json

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

RABBIT_CONNECTION_URL = os.environ['RABBITMQ_URL']

RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL, heartbeat=0,
                                                                        blocked_connection_timeout=300))

EXCHANGE_CHANNEL = RABBITMQ_CONNECTION.channel()
EXCHANGE_CHANNEL.exchange_declare(exchange='vacations',
                                  exchange_type='fanout')


def create_vacation_db(created_by, country, duration, cost, date_of_departure):
    vac = Vacation(created_by=created_by, country=country, duration=duration, cost=cost,
                   date_of_departure=date_of_departure)
    try:
        session.add(vac)
        session.commit()
        body = vac.as_dict()
        body['method'] = 'post'
        EXCHANGE_CHANNEL.basic_publish('vacations', routing_key='', body=json.dumps(body, default=str))
    except SQLAlchemyError as e:
        print(e)
        session.rollback()
        raise e
    return vac.id


def update_vacation_db(vacation_id, owner_login, country = '', duration = 0, cost = 0, date_of_departure = 0):
    select_query = select(Vacation).where(Vacation.id == vacation_id)
    vacation = session.execute(select_query).fetchone()
    if not vacation:
        return
    vacation = vacation[0]
    if not vacation.created_by == owner_login:
        raise ValueError('Only owner can delete a vacation')
    if country:
        vacation.country = country
    if duration:
        vacation.duration = duration
    if cost:
        vacation.cost = cost
    if date_of_departure:
        vacation.date_of_departure = date_of_departure
    try:
        session.commit()
        body = vacation.as_dict()
        body['method'] = 'put'
        EXCHANGE_CHANNEL.basic_publish('vacations', routing_key='', body=json.dumps(body, default=str))
    except SQLAlchemyError as e:
        print(e)
        session.rollback()
        raise e
    return vacation.as_dict()

def delete_vacation_db(vacation_id, login):
    select_query = select(Vacation).where(Vacation.id == vacation_id)
    vacation = session.execute(select_query).fetchone()
    if not vacation:
        return
    vacation = vacation[0]
    if not vacation.created_by == login:
        raise ValueError('Only owner can delete a vacation')
    session.delete(vacation)
    try:
        session.commit()
        body = vacation.as_dict()
        body['method'] = 'delete'
        EXCHANGE_CHANNEL.basic_publish('vacations', routing_key='', body=json.dumps(body, default=str))
        return vacation.as_dict()
    except SQLAlchemyError as e:
        print(e)
        session.rollback()
        raise e
        # response.status_code = status.HTTP_500_INTERNAL_SERVER_ERROR
        # return 'Database error'


def get_vacations_db(offset = 0, limit = 10) -> list[dict]:
    select_query = select(Vacation).offset(offset).limit(limit)
    vacations = session.execute(select_query)
    result = []
    for vacation in vacations:
        result.append(vacation[0].as_dict())
    return result

def get_vacation_by_id_db(vacation_id) -> dict:
    select_query = select(Vacation).where(Vacation.id == vacation_id)
    vacation = session.execute(select_query).fetchone()
    if not vacation:
        return
    return vacation[0].as_dict()