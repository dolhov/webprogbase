import sys
sys.path.append('/home/axel/Desktop/uni/webprogbase/labs/lab6-python/microservices/auth_microservice')

from models_auth.user import User
from models_auth.db_session import session
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy import select
import pika
import json

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

RABBIT_CONNECTION_URL = os.environ['RABBITMQ_URL']


RABBITMQ_CONNECTION = pika.BlockingConnection(pika.ConnectionParameters(RABBIT_CONNECTION_URL, heartbeat=0,
                                                                        blocked_connection_timeout=300))

EXCHANGE_CHANNEL = RABBITMQ_CONNECTION.channel()
EXCHANGE_CHANNEL.exchange_declare(exchange='new_auth',
                                  exchange_type='fanout')


def add_user(login, password, registeredAt):
    usr = User(login=login, password=password, registeredAt=registeredAt)
    try:
        session.add(usr)
        session.commit()
        EXCHANGE_CHANNEL.basic_publish('new_auth', routing_key='', body=json.dumps(usr.as_dict(), default=str))
        return usr.id
    except SQLAlchemyError as e:
        print(e)
        session.rollback()
        raise ValueError('User with such login is already exists')
        # response.status_code = status.HTTP_409_CONFLICT
        # return 'User with such login is already exists'

def get_user(login):
    select_query = select(User).where(User.login == login)
    try:
        usr = session.execute(select_query).fetchone()[0]
        return usr
    except TypeError:
        return