from fastapi import FastAPI, Response, status, Header
import graphene
from starlette.graphql import GraphQLApp
from graphql import GraphQLError
import bcrypt
import jwt
from auth_db_handler import get_user, add_user
from vacations_db_handler import get_vacations_db, get_vacation_by_id_db, create_vacation_db, \
    update_vacation_db, delete_vacation_db
from votes_db_handler import create_vote_db, delete_vote_db
from query_db_handler import get_votes_users_vacations, get_vote_user_vacation, get_vacations_user_voted_for
from sqlalchemy.exc import SQLAlchemyError
import datetime

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

SECRET_SALT = os.environ['SECRET_SALT']


app = FastAPI()

class CreateUser(graphene.Mutation):
    class Arguments:
        login = graphene.String()
        password = graphene.String()

    ok = graphene.Boolean()
    user = graphene.Field(lambda: User_graphql)

    def mutate(root, info, login, password):
        hashed_password = bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt())
        registeredAt = datetime.datetime.now()
        try:
            id = add_user(login, hashed_password.decode('utf-8'), registeredAt)
        except SQLAlchemyError as e:
            return GraphQLError(str(e))
        user = User_graphql(id=id, login=login, password=hashed_password.decode('utf-8'), registeredAt=registeredAt)
        ok = True
        return CreateUser(user=user, ok=ok)


class CreateVacation(graphene.Mutation):
    class Arguments:
        country = graphene.String()
        cost = graphene.Int()
        duration = graphene.Int()
        date_of_departure = graphene.Date()
        jwt_token = graphene.String()

    ok = graphene.Boolean()
    vacation = graphene.Field(lambda: Vacation_graphql)

    def mutate(root, info, country, cost, duration, date_of_departure, jwt_token):
        if cost <= 0 or duration <= 0:
            raise GraphQLError('Incorrect cost or duration')
        try:
            id_login_dict = verify_jwt(jwt_token)
        except ValueError as e:
            raise GraphQLError(str(e))
        try:
            vacation_id = create_vacation_db(id_login_dict['login'], country, duration, cost, date_of_departure)
        except SQLAlchemyError:
            raise GraphQLError('Internal error')
        ok = True
        return CreateVacation(vacation=Vacation_graphql(id=vacation_id, country=country, duration=duration,
                         cost=cost, created_by=id_login_dict['login'],
                         date_of_departure=date_of_departure), ok=ok)

class UpdateVacation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        jwt_token = graphene.String()
        country = graphene.String(required=False, default_value=None)
        cost = graphene.Int(required=False, default_value=None)
        duration = graphene.Int(required=False, default_value=None)
        date_of_departure = graphene.Date(required=False, default_value=None)


    ok = graphene.Boolean()
    vacation = graphene.Field(lambda: Vacation_graphql)

    def mutate(root, info, id, jwt_token, country='', cost=0, duration=0, date_of_departure='', ):
        try:
            id_login_dict = verify_jwt(jwt_token)
        except ValueError as e:
            raise GraphQLError(str(e))
        try:
            vacation_dict = update_vacation_db(id, id_login_dict['login'], country, duration, cost, date_of_departure)
        except ValueError as e:
            raise GraphQLError(str(e))
        except SQLAlchemyError as e:
            raise GraphQLError("Internal error")
        ok = True
        vacation_graph = Vacation_graphql(id=vacation_dict['id'], country=vacation_dict['country'],
                                               duration=vacation_dict['duration'], cost=vacation_dict['cost'],
                                               created_by=vacation_dict['created_by'],
                                               date_of_departure=vacation_dict['date_of_departure'])
        print(vacation_graph.country)
        return UpdateVacation(vacation=vacation_graph, ok=ok)


class DeleteVacation(graphene.Mutation):
    class Arguments:
        id = graphene.ID()
        jwt_token = graphene.String()


    ok = graphene.Boolean()
    vacation = graphene.Field(lambda: Vacation_graphql)

    def mutate(root, info, id, jwt_token):
        try:
            id_login_dict = verify_jwt(jwt_token)
        except ValueError as e:
            raise GraphQLError(str(e))
        try:
            vacation_dict = delete_vacation_db(id, id_login_dict['login'])
        except ValueError as e:
            raise GraphQLError(str(e))
        except SQLAlchemyError as e:
            raise GraphQLError("Internal error")
        ok = True
        vacation_graph = Vacation_graphql(id=vacation_dict['id'], country=vacation_dict['country'],
                                               duration=vacation_dict['duration'], cost=vacation_dict['cost'],
                                               created_by=vacation_dict['created_by'],
                                               date_of_departure=vacation_dict['date_of_departure'])
        return DeleteVacation(vacation=vacation_graph, ok=ok)


class CreateVote(graphene.Mutation):
    class Arguments:
        id_of_resource = graphene.ID()
        type_of_resource = graphene.String()
        jwt_token = graphene.String()

    ok = graphene.Boolean()
    vote = graphene.Field(lambda: Vote_graphql)

    def mutate(root, info, id_of_resource, type_of_resource, jwt_token):
        try:
            id_login_dict = verify_jwt(jwt_token)
        except ValueError as e:
            raise GraphQLError(str(e))
        vote = create_vote_db(id_login_dict['login'], id_of_resource, type_of_resource, datetime.datetime.now())
        vote_graph = Vote_graphql(id=vote['id'], voted_by=vote['voted_by'],
                                  id_of_resource=vote['id_of_resource'],
                                  time_of_vote=vote['time_of_vote'], type_of_resource=vote['type_of_resource'])
        return CreateVote(vote=vote_graph, ok=True)

class DeleteVote(graphene.Mutation):
    class Arguments:
        id_of_resource = graphene.ID()
        jwt_token = graphene.String()

    ok = graphene.Boolean()
    vote = graphene.Field(lambda: Vote_graphql)

    def mutate(root, info, id_of_resource, jwt_token):
        try:
            id_login_dict = verify_jwt(jwt_token)
        except ValueError as e:
            raise GraphQLError(str(e))
        vote = delete_vote_db(id_of_resource, id_login_dict['login'])
        if not vote:
            raise GraphQLError('You are not owner')
        vote_graph = Vote_graphql(id=vote['id'], voted_by=vote['voted_by'],
                                  id_of_resource=vote['id_of_resource'],
                                  time_of_vote=vote['time_of_vote'], type_of_resource=vote['type_of_resource'])
        return DeleteVote(vote=vote_graph, ok=True)


class User_graphql(graphene.ObjectType):
    id = graphene.ID()
    login = graphene.String()
    password = graphene.String()
    registeredAt = graphene.DateTime()

class Vacation_graphql(graphene.ObjectType):
    id = graphene.ID()
    country = graphene.String()
    cost = graphene.Int()
    duration = graphene.Int()
    created_by = graphene.String()
    date_of_departure = graphene.Date()

class Vote_graphql(graphene.ObjectType):
    id = graphene.ID()
    voted_by = graphene.String()
    id_of_resource = graphene.ID()
    time_of_vote = graphene.DateTime()
    type_of_resource = graphene.String()


class Vote_with_user(graphene.ObjectType):
    vote = graphene.Field(Vote_graphql)
    user = graphene.Field(User_graphql)


class Vacation_with_votes(graphene.ObjectType):
    vacation = graphene.Field(Vacation_graphql)
    votes = graphene.List(Vote_with_user)


class MyMutations(graphene.ObjectType):
    create_user = CreateUser.Field()
    create_vacation = CreateVacation.Field()
    update_vacation = UpdateVacation.Field()
    delete_vacation= DeleteVacation.Field()
    create_vote = CreateVote.Field()
    delete_vote = DeleteVote.Field()


class Query(graphene.ObjectType):
    user = graphene.Field(User_graphql)
    login = graphene.String(login=graphene.String(), password=graphene.String())
    vacations = graphene.List(Vacation_graphql)
    vacation = graphene.Field(Vacation_graphql, id=graphene.ID(required=True))
    vacations_with_votes = graphene.List(Vacation_with_votes, offset=graphene.Int(default_value=0),
                                          limit=graphene.Int(default_value=10))
    vacation_with_votes = graphene.Field(Vacation_with_votes, id=graphene.ID(required=True))
    vacations_user_voted_for = graphene.List(Vacation_with_votes, id=graphene.ID(required=True), offset=graphene.Int(default_value=0),
                                          limit=graphene.Int(default_value=10))
    vacations_i_voted_for = graphene.List(Vacation_with_votes, jwt_token=graphene.String(required=True), offset=graphene.Int(default_value=0),
                                          limit=graphene.Int(default_value=10))
    # vacations = graphene.String()

    def resolve_login(self, info, login, password):
        usr = get_user(login)
        if not usr:
            raise GraphQLError('Non-existing login')
        if not bcrypt.checkpw(password.encode('utf-8'), usr.password.encode('utf-8')):
            return GraphQLError('Wrong password')
        encoded_jwt = jwt.encode({'id': usr.id, "login": usr.login}, SECRET_SALT, algorithm="HS256")
        # TODO Добавить время истекания токена
        return encoded_jwt

    def resolve_vacations(self, info, offset = 0, limit = 10):
        vacations = get_vacations_db()
        result = []
        print(vacations)
        for vacation in vacations:
            result.append(Vacation_graphql(id=vacation['id'], country=vacation['country'], cost=vacation['cost'],
                             duration=vacation['duration'], created_by=vacation['created_by'],
                             date_of_departure=vacation['date_of_departure'] ))
        return result

    def resolve_vacation(self, info, id):
        try:
            vacation_dict = get_vacation_by_id_db(id)
        except SQLAlchemyError:
            raise GraphQLError('Internal error')
        return Vacation_graphql(id=vacation_dict['id'], country=vacation_dict['country'],
                                               duration=vacation_dict['duration'], cost=vacation_dict['cost'],
                                               created_by=vacation_dict['created_by'],
                                               date_of_departure=vacation_dict['date_of_departure'])

    def resolve_vacations_with_votes(root, info, offset, limit):
        res = get_votes_users_vacations(offset, limit)
        votes_for_vacations = {}
        for i in res:
            # i == [(Vote, User, Vacation)]
            user_graph = User_graphql(id = i[1].id, login = i[1].login)
            vote_graph = Vote_graphql(id=i[0].id, time_of_vote=i[0].time_of_vote)
            vote_user = Vote_with_user(vote=vote_graph, user=user_graph)
            if i[2].id not in votes_for_vacations:
                vac_grahp = Vacation_graphql(id=i[2].id, country=i[2].country, )
                vacation_with_votes = Vacation_with_votes(vacation=vac_grahp, votes=[])
                votes_for_vacations[i[2].id] = vacation_with_votes
                votes_for_vacations[i[2].id].votes.append(vote_user)
            else:
                votes_for_vacations[i[2].id].votes.append(vote_user)
        final_result = []
        for i in votes_for_vacations:
            final_result.append(votes_for_vacations[i])
        return final_result

    def resolve_vacation_with_votes(self, info, id):
        res = get_vote_user_vacation(id)
        votes_for_vacations = {}
        for i in res:
            # i == [(Vote, User, Vacation)]
            user_graph = User_graphql(id=i[1].id, login=i[1].login)
            vote_graph = Vote_graphql(id=i[0].id, time_of_vote=i[0].time_of_vote)
            vote_user = Vote_with_user(vote=vote_graph, user=user_graph)
            if i[2].id not in votes_for_vacations:
                vac_grahp = Vacation_graphql(id=i[2].id, country=i[2].country, )
                vacation_with_votes = Vacation_with_votes(vacation=vac_grahp, votes=[])
                votes_for_vacations[i[2].id] = vacation_with_votes
                votes_for_vacations[i[2].id].votes.append(vote_user)
            else:
                votes_for_vacations[i[2].id].votes.append(vote_user)
        final_result = []
        for i in votes_for_vacations:
            final_result.append(votes_for_vacations[i])
        if not final_result:
            raise GraphQLError("Id not found")
        return final_result[0]

    def resolve_vacations_user_voted_for(self, info, id, offset, limit):
        res = get_vacations_user_voted_for(id, offset, limit)
        votes_for_vacations = {}
        for i in res:
            # i == [(Vote, User, Vacation)]
            user_graph = User_graphql(id=i[1].id, login=i[1].login)
            vote_graph = Vote_graphql(id=i[0].id, time_of_vote=i[0].time_of_vote)
            vote_user = Vote_with_user(vote=vote_graph, user=user_graph)
            if i[2].id not in votes_for_vacations:
                vac_grahp = Vacation_graphql(id=i[2].id, country=i[2].country, )
                vacation_with_votes = Vacation_with_votes(vacation=vac_grahp, votes=[])
                votes_for_vacations[i[2].id] = vacation_with_votes
                votes_for_vacations[i[2].id].votes.append(vote_user)
            else:
                votes_for_vacations[i[2].id].votes.append(vote_user)
        final_result = []
        for i in votes_for_vacations:
            final_result.append(votes_for_vacations[i])
        if not final_result:
            raise GraphQLError("Id not found")
        return final_result

    def resolve_vacations_i_voted_for(self, info, jwt_token, offset, limit):
        try:
            id_login_dict = verify_jwt(jwt_token)
        except ValueError as e:
            raise GraphQLError(str(e))

        res = get_vacations_user_voted_for(id_login_dict['id'], offset, limit)
        votes_for_vacations = {}
        for i in res:
            # i == [(Vote, User, Vacation)]
            user_graph = User_graphql(id=i[1].id, login=i[1].login)
            vote_graph = Vote_graphql(id=i[0].id, time_of_vote=i[0].time_of_vote)
            vote_user = Vote_with_user(vote=vote_graph, user=user_graph)
            if i[2].id not in votes_for_vacations:
                vac_grahp = Vacation_graphql(id=i[2].id, country=i[2].country, )
                vacation_with_votes = Vacation_with_votes(vacation=vac_grahp, votes=[])
                votes_for_vacations[i[2].id] = vacation_with_votes
                votes_for_vacations[i[2].id].votes.append(vote_user)
            else:
                votes_for_vacations[i[2].id].votes.append(vote_user)
        final_result = []
        for i in votes_for_vacations:
            final_result.append(votes_for_vacations[i])
        if not final_result:
            raise GraphQLError("Id not found")
        return final_result


def verify_jwt(encoded_jwt):
    try:
        id_login_dict = jwt.decode(encoded_jwt, SECRET_SALT, algorithms=["HS256"])
    except jwt.ExpiredSignatureError as e:
        raise ValueError('JWT token expired')
    except jwt.DecodeError as e:
        raise ValueError('Invalid JWT token')
    return id_login_dict


app.add_route("/", GraphQLApp(schema=graphene.Schema(query=Query, mutation=MyMutations)))