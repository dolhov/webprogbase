from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, DateTime
from .db_session import engine

Base = declarative_base()

class Vote(Base):
    __tablename__ = 'votes'

    id = Column(Integer, primary_key=True)
    voted_by = Column(String)
    id_of_resource = Column(Integer)
    time_of_vote = Column(DateTime)
    type_of_resource = Column(String)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


if __name__ == '__main__':
    # from sqlalchemy.orm import sessionmaker

    # USER = Config.USER
    # PASSWORD = Config.PASSWORD
    # DB = Config.DB
    # HOST = Config.HOST

    # engine = create_engine(POSTRESQL_DATABASE_URL, echo=True)
    # session = sessionmaker()
    # session.configure(bind=engine)
    Base.metadata.create_all(engine)