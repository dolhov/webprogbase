from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker, Session

import os
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

POSTGRESQL_DATABASE_URL = os.environ['QUERY_DB_URL']


engine = create_engine(POSTGRESQL_DATABASE_URL, echo=False)
session = Session(bind=engine)
