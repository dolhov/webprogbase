import datetime

class View:
    def __init__(self, answers_handler):
        self.handler = answers_handler

    def list_of_all_rooms(self):
        i = 1
        for room in self.handler.chat_rooms:
            print(f'[{i}) {room["name"]}]')
            i += 1

    def members_list(self):
        print('[Members in this room: ]')
        i = 1
        for member in self.handler.members_in_current_room:
            print(f'[{i}) {member}]')

    @staticmethod
    def member_joined(member):
        print(f'[{member} has joined the room]')

    @staticmethod
    def member_left(member):
        print(f'[{member} has left the room]')

    @staticmethod
    def message_posted(message):
        date_time_obj = datetime.datetime.fromisoformat(message['timestamp'])

        print(f'[{message["author"]}][{date_time_obj.time()}]: {message["text"]}')
