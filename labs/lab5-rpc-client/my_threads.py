import threading
import requests
import json
from messages import create_app_message, MessageTypes
from concurrent.futures import ThreadPoolExecutor


class GetUpdatesThread(threading.Thread):
    def __init__(self, token, server_url):
        super(GetUpdatesThread, self).__init__()
        self.daemon = True
        self.last_user_input = None
        self.response = []
        self.token = token
        self.server_url = server_url

    def run(self):
        while True:
            #print(create_app_message(self.token, MessageTypes.CLIENT_GET_UPDATES, ''))
            try:
                with ThreadPoolExecutor(max_workers=1) as executor:
                    future = executor.submit(requests.post, self.server_url,
                                             json=create_app_message(self.token, MessageTypes.CLIENT_GET_UPDATES, ''),
                                             timeout=60)
                #print('here')
                #if not future.running():
                    #print(future.result())
                # self.response = requests.get(self.server_url,
                #                              json=create_app_message(self.token, MessageTypes.CLIENT_GET_UPDATES, ''),
                #                              timeout=60)
            except requests.exceptions.BaseHTTPError as e:
                print(e)


class SendMessageThread(threading.Thread):
    def __init__(self, server_url):
        super(SendMessageThread, self).__init__()
        self.daemon = True
        self.last_user_input = None
        self.send_message = []
        self.response = None
        self.server_url = server_url

    def run(self):
        while True:
            if self.send_message:
                for i in self.send_message:
                    try:
                        # print('I send this message: ', json.dumps(i))
                        self.response = requests.post(self.server_url, json=i, timeout=5)
                        # print(self.response)
                        self.send_message.remove(i)
                    except TimeoutError:
                        continue


# define a thread which takes input
# class InputThread(threading.Thread):
#     def __init__(self):
#         super(InputThread, self).__init__()
#         self.daemon = True
#         self.last_user_input = None
#
#     def run(self):
#         while True:
#             self.last_user_input = input('Your input: ')
#             print('You have entered data')
#             # do something based on the user input here
#             # alternatively, let main do something with
#             # self.last_user_input