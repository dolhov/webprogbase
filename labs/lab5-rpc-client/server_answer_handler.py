from view import View
import json

class ServerAnswerHandler:

    def __init__(self):
        self.ALLOWED_TYPE = {'rooms-list': self.rooms_list, 'room-created': self.room_created,
                             'room-renamed': self.room_renamed, 'room-removed': self.room_removed,
                             'current-room-changed': self.current_room_changed, 'members-list': self.members_list,
                             'member-joined': self.member_joined, 'member-left': self.member_left,
                             'last-messages-list': self.last_messages_list, 'message-posted': self.message_posted}
        self.chat_rooms = []
        self.current_chat_room = {}
        self.members_in_current_room = []
        self.last_messages = []
        self.view = View(self)
        self.my_login = ''

    def handle_messages(self, messages: []):
        if not messages:
            return
        for i in messages:
            j = i
            if isinstance(i, str):
                j = json.loads(i)
            self.ALLOWED_TYPE[j['type']](j)


    def rooms_list(self, app_message):
        for i in app_message['payload']:
            self.chat_rooms.append(i)
        self.view.list_of_all_rooms()

    def room_created(self, app_message):
        self.chat_rooms.append(app_message['payload'])
        if not self.current_chat_room:
            print('New room created: ')
            self.view.list_of_all_rooms()

    def room_renamed(self, app_message):
        index = -1
        for index_loop, i in enumerate(self.chat_rooms):
            if i['name'] == app_message['payload']['oldRoomName']:
                index = index_loop
        # index = self.chat_rooms.index(app_message['payload']['oldRoomName'])
        if not index >= 0 or not index < len(self.chat_rooms):
            return
        self.chat_rooms[index]['name'] = app_message['payload']['newRoomName']
        if self.current_chat_room == app_message['payload']['oldRoomName']:
            self.current_chat_room = app_message['payload']['newRoomName']
            self.view.list_of_all_rooms()
        elif not self.current_chat_room:
            print('Room renamed: ')
            self.view.list_of_all_rooms()

    def room_removed(self, app_message):
        room_name = app_message['payload']
        self.chat_rooms.remove(room_name)
        if room_name != self.current_chat_room:
            print('Room removed: ')
            self.view.list_of_all_rooms()
        else:
            print('Your room was removed by the owner.')
            self.view.list_of_all_rooms()

    def current_room_changed(self, app_message):
        self.current_chat_room = app_message['payload']

    def members_list(self, app_message):
        for member in app_message['payload']:
            if member not in self.members_in_current_room:
                self.members_in_current_room.append(member)
        self.view.members_list()

    def member_joined(self, app_message):
        member = app_message['payload']
        if member != self.my_login:
            self.members_in_current_room.append(app_message['payload'])
        View.member_joined(member)

    def member_left(self, app_message):
        self.members_in_current_room.remove(app_message['payload'])
        self.view.member_left(app_message['payload'])

    def last_messages_list(self, app_messages):
        for message in app_messages['payload']:
            self.last_messages.append(message)
            self.view.message_posted(message)

    def message_posted(self, app_message):
        self.last_messages.append(app_message['payload'])
        self.view.message_posted(app_message['payload'])

    def print_rooms(self):
        self.view.list_of_all_rooms()

