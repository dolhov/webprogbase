from messages import create_app_message, MessageTypes
from my_threads import SendMessageThread, GetUpdatesThread # InputThread
from server_answer_handler import ServerAnswerHandler
from multiprocessing import Process, Queue
import requests
import time
import threading
import simplejson.errors

server_answer_handler = ServerAnswerHandler()


SERVER_URL = 'https://still-mesa-73593.herokuapp.com/rpc'

TOKEN = ''





def get_updates(q: Queue):
    while True:
        # print(create_app_message(TOKEN, MessageTypes.CLIENT_GET_UPDATES, ''))
        try:
            response = requests.post(SERVER_URL,
                                    json=create_app_message(TOKEN, MessageTypes.CLIENT_GET_UPDATES, ''),
                                    timeout=60)
            # print(f'Response: {response} {type(response.json())}')
            responses = response.json()
            # print('Responses type: ', type(responses))
            if response:
                q.put(responses)
        except requests.exceptions.BaseHTTPError as e:
            print(e)
        except simplejson.errors.JSONDecodeError:
            continue

def get_input(q: Queue):
    while True:
        last_user_input = input('')
        q.put(last_user_input)

def join_room(user_input, send_message_thread, token):
    try:
        commands = input_user.split()
        room_number = int(commands[1])
        if room_number < 0 or room_number > len(server_answer_handler.chat_rooms):
            raise ValueError
    except IndexError:
        print('You haven\'t specified the room to join')
        return False
    except ValueError:
        print('Incorrect room number')
        return False
    room_name = server_answer_handler.chat_rooms[room_number - 1]['name']
    msg_join_room = create_app_message(token, MessageTypes.CLIENT_JOIN_ROOM, room_name)
    msg_get_members_in_room = create_app_message(token, MessageTypes.CLIENT_GET_MEMBERS_LIST, room_name)
    msg_get_last_messages_in_room = create_app_message(token, MessageTypes.CLIENT_GET_LAST_MESSAGES_LIST, room_name)
    send_message_thread.send_message.extend([msg_join_room, msg_get_members_in_room, msg_get_last_messages_in_room])
    return True


# msg = it.last_user_input
# it.last_user_input = None
# main


if __name__ == "__main__":
    login = input('Enter login: ')
    telegram_login = input('Enter telegram login: ')
    server_answer_handler.my_login = login
    # it = InputThread()
    # it.start()
    send_message_thread = SendMessageThread(SERVER_URL)
    send_message_thread.start()
    send_message_thread.send_message.append(create_app_message('', MessageTypes.CLIENT_GET_TOKEN,
                                                               {'login': login, 'telegramLogin': telegram_login}))
    while not send_message_thread.response:
        continue
    server_answer = send_message_thread.response.json()
    if server_answer['type'] == 'token':
        TOKEN = server_answer['payload']

    send_message_thread.send_message.append(create_app_message(TOKEN, MessageTypes.CLIENT_GET_ROOMS_LIST, ''))
    # gut = GetUpdatesThread(TOKEN, SERVER_URL)
    # gut.run()
    q = Queue()
    user_input_queue = Queue()

    it = threading.Thread(target=get_input, args=(user_input_queue, ))
    it.start()

    get_update_process = Process(target=get_updates, args=(q,), daemon=True)
    get_update_process.start()
    while True:
        if not q.empty():
            messages = q.get()
            # print(messages)
            if messages is not None:
                server_answer_handler.handle_messages(messages)
        if not user_input_queue.empty():
            input_user = str(user_input_queue.get())
            if input_user == '/exit':
                break
            elif input_user.startswith('/join '):
                res = join_room(input_user, send_message_thread, TOKEN)
                if not res:
                    continue
            elif input_user == '/leave':
                msg = create_app_message(TOKEN, MessageTypes.CLIENT_LEAVE_ROOM, server_answer_handler.current_chat_room)
                send_message_thread.send_message.append(msg)
                server_answer_handler.current_chat_room = ''
                server_answer_handler.members_in_current_room.clear()
            elif input_user == '/list_rooms':
                server_answer_handler.print_rooms()
            elif server_answer_handler.current_chat_room:
                msg = create_app_message(TOKEN, MessageTypes.CLIENT_POST_MESSAGE, input_user)
                send_message_thread.send_message.append(msg)
            # print(msg)



    print('Exit worked')
    get_update_process.kill()

