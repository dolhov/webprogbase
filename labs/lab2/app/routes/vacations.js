const express = require('express');
const router = express.Router();
const vacationController = require('../controllers/vacation_controller.js');
const Vacation = require('../models/vacation.js');



/**
 * get all vacations (pagination possible)
 * @route GET /api/vacations
 * @group Vacations - vacation operations
 * @param {integer} page.query - page number
 * @param {integer} limit.query - items per page
 * @returns {Array.<Vacation>} 200 - all vacations
 */
router.get("/", vacationController.getVacations);


/**
 * get  vacations
 * @route GET /api/vacations/{id}
 * @group Vacations - vacation operations
 * @param {integer} id.path.required - id of vacation
 * @returns {integer} 200 - vacation with the id
 * @returns {Error} 404 - not found
 */
router.get("/:id", vacationController.getVacation);


/**
 * Insert vacation to DB
 * @route POST /api/vacations
 * @group Vacations - vacations operations
 * @consumes multipart/form-data
 * @param {file} vacation.formData.required - uploaded json vacation
 * @returns {string} 200 - added json vacation
 * @returns {error} 400 - bad request
 */

router.post("/", vacationController.postVacation)

/**
 * Update vacation in DB
 * @route PUT /api/vacations
 * @group Vacations - vacations operations
 * @consumes multipart/form-data
 * @param {file} vacation.formData.required - uploaded json vacation
 * @returns {string} 200 - updated json vacation
 * @returns {Error} 404 - updated json vacation
 * @returns {error} 400 - bad request
 */

router.put("/", vacationController.updateVacation)

/**
 * delete  vacations 
 * @route DELETE /api/vacations/{id}
 * @group Vacations - vacation operations
 * @param {integer} id.path.required - id of vacation
 * @returns {vacation} 200 - delete vacation
 */

router.delete("/:id", vacationController.deleteVacation)


module.exports = router;
