const express = require('express');
const media_controller = require("../controllers/media_controller")
const router = express.Router();


/**
 * get image by its id
 * @route GET /api/media/{id}
 * @group Media - media upload and get images
 * @param {integer} id.path.required - id of the User - eg: 1
 * @returns {file} 200 - Image
 * @returns {Error} 404 - Image not found
 */

router.get("/:id", media_controller.getImage)


/**
 * Post image
 * @route POST /api/media
 * @group Media - upload and get images
 * @consumes multipart/form-data
 * @param {file} image.formData.required - uploaded image
 * @returns {integer} 200 - added image id
 */

router.post("/", media_controller.postImage)

module.exports = router;