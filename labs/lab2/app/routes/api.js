const router = require('express').Router();

const userRouter = require('./users');
const vactionRouter = require('./vacations')
const mediaRouter = require('./media')
router.use('/users', userRouter);
router.use('/vacations', vactionRouter)
router.use('/media', mediaRouter)

module.exports = router;
