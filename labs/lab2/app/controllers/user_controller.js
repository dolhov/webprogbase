

const UserRepository = require('./../repositories/user_repository');
 
const userRepository = new UserRepository("data/users.json");
 
module.exports = {
 
    getUsers(req, res) {
        let limit = req.query.limit
        let page = req.query.page
        if (page < 1) {
            res.status(400).send("Bad request")
        }
        const users = userRepository.getUsers(limit, (page - 1) * limit);
        let us = [];
        for (i = 0; i < users.length; i++) {
            let dc = {}
            us += `\"id\": ${users[i]["id"]}, "fullname": ${users[i]["fullname"]}\n`;
        }
        res.send(us);
    },
 
    getUser(req, res) {
        const userId = parseInt(req.params.id);
        const user = userRepository.getUserById(userId);
        if (!user) {
            res.status(404).send(`Not found`);
            return;
        }
        let ans = "";
        for (key in user) {
            ans += key + ": " + user[key] + "\n"
        }
        res.send(ans);
    },
};
