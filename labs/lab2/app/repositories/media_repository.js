const fs = require('fs');

class mediaRepository {


    postMedia(media) {
        const str_id = fs.readFileSync("./media/id.txt");
        const id = parseInt(str_id)
        fs.writeFileSync("./media/id.txt", String(id + 1))
        fs.writeFileSync(`./media/${id}`, media)
        return id
    }

    getMedia(id) {
        try {
            let pic = fs.readFileSync(`./media/${id}.png`);
            return pic
        }
        catch {
            return
        }
    }
}

module.exports = mediaRepository