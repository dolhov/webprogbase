const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
 
class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
    }
 
    getUsers(limit, offset) { 
        const items = this.storage.readItems();
        let ans = []
        for (let i = offset; i < limit + offset; i++) {
            if (i < items["items"].length)
            {
                ans.push(items["items"][i])
            }
            else {
                break
            }
        }
        return ans;
        // throw new Error("Not implemented"); 
    }
 
    getUserById(id) {
        const items = this.storage.readItems();
        // console.log(typeof(items));
        for (const item of items["items"]) {
            if (item.id === id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }
};
 
module.exports = UserRepository;

