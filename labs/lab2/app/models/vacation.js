

/**
 * @typedef Vacation
 * @property {integer} id
 * @property {string} tour_operator.required - tour operator, who provides vacation
 * @property {string} country.required - to which counry vacation proposes a trip 
 * @property {integer} cost.required - cost
 * @property {string} date - date of departure
 */


class Vacation {

    constructor (id, tour_operator, country, duration, cost, date) {
        this.id = id;
        this.tour_operator = tour_operator;
        this.country = country;
        this.duration = duration;
        this.cost = cost;
        this.date = date;
    }
}



module.exports = Vacation;