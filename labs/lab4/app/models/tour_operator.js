const mongoose = require('mongoose')


const { Schema } = mongoose;

/**
 * @typedef TOurOperator
 * @property {string} _id 
 * @property {string} name.required - unique tour operator name
 * @property {string} country_of_registration.required - country, where company is registered
 * @property {array} accepted_currency.required - currency which company accepts
 * @property {string} description - descitption of the company
 * @property {string} avaUrl - link to the avatar pic
 * @property {integer} average_tour_price - [optional] average price of tour
 */

const tourOperatorSchema = new Schema({
    name:  String, // String is shorthand for {type: String}
    country_of_registration : String,
    accepted_currency: [String],
    description: String,
    avaUrl: String,
    average_tour_price: Number
});

const TourOperatorForMongo = mongoose.model('tour_operators', tourOperatorSchema);

module.exports = TourOperatorForMongo