const mongoose = require('mongoose')


/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required - unique username
 * @property {string} fullname.required - fullname
 * @property {bool} role.required - 0 - simple user, 1 - Admin
 * @property {string} registeredAt - date
 * @property {string} avaUrl - link to the avatar pic
 * @property {bool} isEnabled - 0 - accout disactivated, no actions permitted, 1 - activated
 */

class User {

    constructor(id, login, fullname, role, registeredAt, avaUrl, isEnabled) {
        this.id = id;  // number
        this.login = login; // string
        this.fullname = fullname;  // string
        this.role = role; // bool
        this.registeredAt = registeredAt; // date ISO 8601
        this.avaUrl = avaUrl; // string with url of avatar
        this.isEnabled = isEnabled; // bool, is user is activated (via email?)

        // TODO: More fields
    }
 };


const { Schema } = mongoose;

const userSchema = new Schema({
    login:  String, // String is shorthand for {type: String}
    fullname: String,
    role:   Boolean,
    registeredAt: Date,
    isEnabled: Boolean,
    avaUrl: String,
    vacations: [{type: mongoose.Schema.Types.ObjectId, 
      ref: "vacations" }]
  });
 
const UserForMongo = mongoose.model('users', userSchema);
module.exports = {User, UserForMongo};
 