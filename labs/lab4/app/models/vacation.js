const mongoose = require('mongoose');

/**
 * @typedef Vacation
 * @property {integer} id
 * @property {string} tour_operator.required - tour operator, who provides vacation
 * @property {string} country.required - to which counry vacation proposes a trip 
 * @property {integer} cost.required - cost
 * @property {string} date - date of departure
 */


class Vacation {

    constructor (id, tour_operator, country, duration, cost, date, image = "") {
        this.id = id;
        this.tour_operator = tour_operator;
        this.country = country;
        this.duration = duration;
        this.cost = cost;
        this.date = date;
        this.image = image
    }
}


  const { Schema } = mongoose;

  const vacationSchema = new Schema({
    tour_operator:  {type: Schema.Types.ObjectId, 
                      ref: "tour_operators", 
                      required: true}, 
    country: String,
    duration: Number,
    cost: Number,
    date: Date,
    image: String
  });


const VacationForMongo = mongoose.model('vacations', vacationSchema);

module.exports = {Vacation, VacationForMongo};