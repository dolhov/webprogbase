const express = require('express');
const router = express.Router();
const userController = require('../controllers/user_controller.js');

/**
 * get all users (pagination possible)
 * @route GET /api/users
 * @group Users - user operations
 * @param {integer} page.query - page number
 * @param {integer} limit.query - items per page
 * @returns {Array.<User>} 200 - all users
 */


router.get("/", userController.getUsers);

/**
 * get user by his id
 * @route GET /api/users/{id}
 * @group Users - user operations
 * @param {string} id.path.required - id of the User - eg: 602ec873dd0d1e64cff49b85
 * @returns {User.model} 200 - User object
 * @returns {integer} 404 - User not found
 * 
 */


router.get("/:id", userController.getUser);

/**
 * Post
 * @route POST /api/users/
 * @group Users - user operations
 * 
 * @param {string} login.query.required - user login
 * @param {string} fullname.query.required - user fullname
 * @param {string} role.query.required - user Role
 * @param {file} avatar.formData - User avatar
 * @returns {User.model} 200 - User object
 * 
 */


router.post("/", userController.postUser);

/**
 * Add vacation to user
 * @route POST /api/users/{id}
 * @group Users - user operations
 * @param {string} id.path.required - id of user
 * @param {string} vacation_id.query.required - uploaded vacation id
 * @returns {User.model} 200 - User object
 * @returns {integer} 404 - User not found
 * 
 */


router.post("/:id", userController.postVacationUser);



module.exports = router;
