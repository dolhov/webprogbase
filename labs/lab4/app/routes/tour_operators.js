const express = require('express');
const tour_operator_controller = require("../controllers/tour_operator_controller.js")
const router = express.Router();

/**
 * get tour_operator with id
 * @route GET /api/operators/{id}
 * @group TourOperators - tour operators operations
 * @param {string} id.path.required - id of vacation
 * @returns {TourOperator} 200 - tour operator
 */
router.get("/:id", tour_operator_controller.get_tour_operator)

/**
 * get all tour operators 
 * @route GET /api/operators/
 * @group TourOperators - tour operators operations
 * @returns {TourOperator} 200 - tour operator
 */
router.get("/", tour_operator_controller.get_all_tour_operators)

/**
 * post tour operator
 * @route POST /api/operators
 * @group TourOperators - tour operators operations
 * @param {file} tour_operator.formData.required - uploaded json tour operator
 * @returns {} 200 - tour operator
 */

router.post("/", tour_operator_controller.post_tour_operator)

/**
 * update
 * @route PUT /api/operators
 * @group TourOperators - tour operators operations
 * @param {file} tour_operator.formData.required - uploaded json tour operator
 * @returns {} 200 - tour operator
 */
router.put("/", tour_operator_controller.update_tour_operator)

/**
 * Delete tour operator 
 * @route Delete /api/operators/{id}
 * @group TourOperators - tour operators operations
 * @param {string} id.path.required - tour operator for deletetion
 * @returns {} 200 - tour operator
 */
router.delete("/:id", tour_operator_controller.delete_tour_operator)

module.exports = router