
require('dotenv').config();
const env = process.env.NODE_ENV;

// const config = {
//     app: {
//       port: 3001
//     },
//     db: {
//       host: 'mongodb+srv://axel:666524@cluster0.j4yq0.mongodb.net/webprogbase',
//       port: 27017,
//       name: 'webprogbase'
//     },
//     cloudinary: {
//       cloud_name: process.env.cloud_name,
//       api_key: parseInt(process.env.api_key),
//       api_secret: process.env.api_secret
//     }
//    };
////////

const dev = {
  app: {
    port: parseInt(process.env.PORT) || 3000
  },
  db: {
    host: process.env.DEV_DB_HOST || 'localhost',
    port: process.env.DEV_DB_PORT || 27017,
    name: process.env.DEV_DB_NAME || 'db'
  },
  cloudinary: {
    cloud_name: process.env.cloud_name,
    api_key: parseInt(process.env.api_key),
    api_secret: process.env.api_secret
    }
 };
 const test = {
  app: {
    port: parseInt(process.env.TEST_APP_PORT) || 3000
  },
  db: {
    host: process.env.TEST_DB_HOST || 'localhost',
    port: parseInt(process.env.TEST_DB_PORT) || 27017,
    name: process.env.TEST_DB_NAME || 'test'
  }
 };
  
 const config = {
  dev,
  test
 };
 
module.exports = config;