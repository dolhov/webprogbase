

const Vacation = require('../models/vacation');
const TourOperator = require('../models/tour_operator');
const JsonStorage = require('../jsonStorage');
const readlineSync = require('readline-sync');
const mongoose = require('mongoose');
const { VacationForMongo } = require('../models/vacation');

function isNumeric(value) {
    return /^\d+$/.test(value);
}

function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
  }

//mongoose.connect('mongodb://localhost/webprogbase', {useNewUrlParser: true, useUnifiedTopology: true});

class VacationRepository {
    
    constructor(filePath) {
        // this.storage = new JsonStorage(filePath);
        this.db  = mongoose.connection;
        this.db.on('error', console.error.bind(console, 'connection error:'));
        this.db.once('open', function() {
            // console.log("Connected to db")
            // console.log(filePath)
        });
    }

    async addVacation(vacation) {
        let tour_operator = await TourOperatorForMongo.findById(vacation["tour_operator"]).exec()
        if (tour_operator) 
        {
            return await VacationForMongo.create(vacation, function (err, res) {
                if (err) {
                     console.error(err)}
                    
                console.log(res)
                return res._id
              });
        }
        else {
            return 
        }
        
    }

    async getVacations(lim, offset, search_name = "") {
        let limit = lim - offset
        let ans = []
        let i = offset
        //console.log(limit, offset, search_name, items["items"].length)
        let j = 0
        let regexp = "^" + search_name
        if (search_name === ""){
            return  Vacation.VacationForMongo.find({}).skip(offset).limit(lim).exec()
        }
        else {
            return  Vacation.VacationForMongo.find({"tour_operator": regexp}).skip(offset).limit(lim).exec()
        }
        
        while(true) {
            if (j < items["items"].length)
            {
                if (search_name) {
                    if (items["items"][j]["tour_operator"].startsWith(search_name)) {
                        if (i > 0) {
                            i -= 1
                        }
                        else if (ans.length < limit){
                            ans.push(items["items"][j])
                        }
                        else {
                            return ans
                        }
                    }
                }
                else {
                    if (i > 0) {
                        i -= 1
                    }
                    else if (ans.length < limit){
                        ans.push(items["items"][j])
                    }
                    else {
                        return ans
                    }
                }
            }
            else {
                break
            }  
            j += 1       
        }
        return ans;
        // throw new Error("Not implemented"); 
    }

    async getVacationsLength(tourop_name="") {
        let regexp = "^" + tourop_name
        if (tourop_name === "") {
            return Vacation.VacationForMongo.countDocuments({}, function (err, res) {
                if (err) {
                    console.log(err)
                }
                return err
            })
        }
        else {
            return Vacation.VacationForMongo.countDocuments({"tour_operator": tourop_name}, function (err, res) {
                if (err) {
                    console.log(err)
                }
                return res
            })
        }
        
        const items = this.storage.readItems();
        let counter = 0
        let i = 0
        while (i < items["items"].length) {

            if (tourop_name) {
                if (items["items"][i]["tour_operator"].startsWith(tourop_name)) {
                    counter += 1
                }
            }
            else {
                counter += 1
            }
            i++
        }
        console.log(counter)
        return counter
    }

    async getVacationById(vacation_id) {
        //const items = this.storage.readItems();
        // console.log(typeof(items));
        // for (const item of items["items"]) {
        //     if (item.id == vacation_id) {
        //         return new Vacation(item.id, item.tour_operator, item.country,
        //             item.duration, item.cost, item.date, item.image);
        //     }
        // }
        return VacationForMongo.findById(vacation_id).exec()
        // throw new Error("Not implemented"); 
    }

    async updateVacation (vacation) {
        let mongo_vacation = await VacationForMongo.findById(vacation["id"])
        mongo_vacation.tour_operator = vacation["tour_operator"]
        mongo_vacation.country = vacation["country"]
        mongo_vacation.duration = vacation["duration"]
        mongo_vacation.cost = vacation["cost"]
        mongo_vacation.date =  vacation["date"]
        return mongo_vacation.save()
        // const items = this.storage.readItems();
        // for (let i = 0; i < items["items"].length; i++) {
        //     if (items["items"][i].id == vacation.id) {
        //         items["items"][i] = vacation;
        //         this.storage.writeChanges(items);
        //         return vacation;
        //     }
        // }
        // return None;
        // throw new Error("Not implemented"); 
    }

    async deleteVacation (vacation_id) {
        return VacationForMongo.deleteOne({"_id": vacation_id}, function (err, res) {
            if (err) {
                console.log(err)
            }
            return res
        })
        // let vc
        // const items = this.storage.readItems();
        // for (let i = 0; i < items["items"].length; i++) {
        //     if (items["items"][i].id === vacation_id) {
        //         vc = items["items"][i]
        //         items["items"].splice(i, 1);
        //         this.storage.writeChanges(items);
        //         return vc;
        //     }
        // }
        // return vc;
    }

    createVacation() {
        let vac = new Vacation();
        vac.tour_operator = readlineSync.question("Enter tour operator name:").trim();
        while (true)
        {
            try {
                let cost = readlineSync.question("Enter cost:").trim()
                if (!isNumeric(cost))
                {
                    throw TypeError("Cost should be entered as positive integer. Try again.")
                }
                vac.cost = Number(cost)
            }
            catch (e) {
                if (e instanceof TypeError) {
                    console.log(e.message)
                    continue
                }
            }
            break
        }
        
        vac.country = readlineSync.question("Enter country: ").trim();
        
        while (true)
        {
            try {
                let dur = readlineSync.question("Enter duration: ").trim();
                if (!isNumeric(dur))
                {
                    throw TypeError("Duration should be entered as positive integer. Try again.")
                }
                vac.duration = Number(dur)
            }
            catch (e) {
                if (e instanceof TypeError) {
                    console.log(e.message)
                    continue
                }
            }
            break
        }
        let date
        while (true) {
            let year = readlineSync.question("Enter year: ").trim()
            let month = readlineSync.question("Enter month (number): ").trim()
            let day = readlineSync.question("Enter day: ").trim()
            let hours = readlineSync.question("Enter hours: ").trim()
            let minutes = readlineSync.question("Enter minutes: ").trim()
            date = new Date(year, month, day, hours, minutes)
            if (isValidDate(date))
            {
                break
            }
            console.log("Invalid date. Try again")
        }
        vac.date = date
        return vac;
    }

    convertVacationToDict(vac) {
        return {'id': vac._id, 'tour_operator': vac.tour_operator, 'country': vac.country, 
                'duration': vac.duration, 'cost': vac.cost, 'date': vac.date, 'image': vac.image}
    }
}

//  const vacRepository = new VacationRepository("data/users.json");
// // userRepository.insertUser(new_user)
// console.log(vacRepository.addVacation({"tour_operator": "TUI", "country": "Turkey",
// "duration": 15, "cost": 155, "date": "2020-11-05", "image": ""}))


module.exports = VacationRepository;