const fs = require('fs');

class viewRepository {

    getIndex() {
        try {
            let index = fs.readFileSync(`./views/index.html`);
            return index
        }
        catch {
            return
        }
    }

    getUsers() {
        try {
            let users = fs.readFileSync(`./views/users.html`);
            return users
        }
        catch {
            return
        }
    }

    getUser(id) { 
        try {
            let users = fs.readFileSync(`./views/user${id}.html`);
            return users
        }
        catch {
            return
        }
    }
}

module.exports = viewRepository