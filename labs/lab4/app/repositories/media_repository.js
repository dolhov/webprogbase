const fs = require('fs').promises;
const config = require('../config');
const cloudinary = require('cloudinary');
cloudinary.config({
    cloud_name: config.dev.cloudinary.cloud_name,
    api_key: config.dev.cloudinary.api_key,
    api_secret: config.dev.cloudinary.api_secret
});


class mediaRepository {


    async postMedia(media) {
        return new Promise((resolve, reject) => {
            cloudinary.v2.uploader
                .upload_stream(
                  { resource_type: 'raw' }, 
                  (err, result) => {
                    if (err) {
                      reject(err);
                    } else {
                      resolve(result);
                    }
                  })
                .end(media);
          });
        
        // const str_id = fs.readFileSync("./data/media/id.txt");
        // const id = parseInt(str_id)
        // fs.writeFileSync("./data/media/id.txt", String(id + 1))
        // fs.writeFileSync(`./data/media/${id}`, media)
        // return id
    }

    getMedia(id) {
        try {
            let pic = fs.readFileSync(`./data/media/${id}`);
            return pic
        }
        catch {
            return
        }
    }
}

module.exports = mediaRepository