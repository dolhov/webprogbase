const User = require('../models/user');
const JsonStorage = require('../jsonStorage');
const mongoose = require('mongoose');
const cons = require('consolidate');
const { UserForMongo } = require('../models/user');
const { VacationForMongo } = require('../models/vacation');


//mongoose.connect('mongodb://localhost/webprogbase', {useNewUrlParser: true, useUnifiedTopology: true});

class UserRepository {
 
    constructor(filePath) {
        this.storage = new JsonStorage(filePath);
        this.db  = mongoose.connection;
        this.db.on('error', console.error.bind(console, 'connection error:'));
        this.db.once('open', function() {
            // console.log("Connected to db")
            // console.log(filePath)
        });
    }

    async insertUser(User) {
        await User.save(function (err, res) {
            if (err) return console.error(err);
            console.log(res)
          });
    }
 
    async getUsers(limit = 100, offset = 0) { 
        return  UserForMongo.find({}).skip(offset).limit(limit).exec()
        const items = this.storage.readItems();
        let ans = []
        for (let i = offset; i < limit + offset; i++) {
            if (i < items["items"].length)
            {
                ans.push(items["items"][i])
            }
            else {
                break
            }
        }
        return ans;
        // throw new Error("Not implemented"); 
    }
 
    async getUserById(id) {
        return  UserForMongo.findById(id).exec()
        const items = this.storage.readItems();
        // console.log(typeof(items));
        for (const item of items["items"]) {
            if (item.id === id) {
                return new User(item.id, item.login, item.fullname, item.role, item.registeredAt, item.avaUrl, item.isEnabled);
            }
        }
        return null;
    }

    async postUser(user) { // user - JSON 
        let new_user = await JSON.parse(user)
        let count = await UserForMongo.countDocuments({ "login": new_user["login"]})
        if (count != 0){
            return {}
        }
        try {
            await UserForMongo.create(new_user, function (err, res) {
                if (err) {
                    console.error(err)}
                else {
                    console.log(res)
                    return res
                }
                
            });
        }
        catch (err) {
            console.log(err)
            throw err
        }
    }

    async postVacationToUser(user_id, vacation_id) {
        let count = await VacationForMongo.countDocuments({ "_id": vacation_id })
        if (count > 0) {
            let usr = await UserForMongo.findById(user_id).exec()
            usr.vacations.push(vacation_id)
            let res = usr.save()
            return res
        }

    }

    convertUserToDict(usr) {
        return {'id': usr.id, 'login': usr.login, 'fullname': usr.fullname, 'role': usr.role, 'registeredAt': usr.registeredAt, 
            'avaUrl': usr.avaUrl, 'isEnabled': usr.isEnabled}
    }
};

// const userRepository = new UserRepository("data/users.json");
// new_user = new User.UserForMongo({"login": "AnotherMongooer", "fullname": "MongoLover1",
//                              "role": false, "registeredAt": "2020-10-05", 
//                              "avaUrl": "", "isEnabled": true, 
//                              "vacations": []})
// userRepository.insertUser(new_user)






module.exports = UserRepository;

