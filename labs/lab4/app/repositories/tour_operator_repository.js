const TourOperatorForMongo = require('../models/tour_operator.js');
const mongoose = require('mongoose');


class TourOperatorRepository {

    constructor() {
        // this.storage = new JsonStorage(filePath);
        this.db  = mongoose.connection;
        this.db.on('error', console.error.bind(console, 'connection error:'));
        this.db.once('open', function() {
            // console.log("Connected to db")
            // console.log(filePath)
        });
    }

    async insert_tour_operator(tour_op) { // tour_op - json
            new TourOperatorForMongo(tour_op).save(function (err, res) {
            if (err) return console.error(err)
            return res
          });
    }

    async get_tour_operator(id) {
        return TourOperatorForMongo.findById(id).exec()
    }

    async get_all_tour_operators() {
        return TourOperatorForMongo.find({ }).exec()
    }

    async update_tour_operator(tour_op) { // tour_op - json
        let dict_operator = await JSON.parse(tour_op)
        try {
            let operator = await TourOperatorForMongo.findById(dict_operator["_id"]).exec()
            operator.name = dict_operator["name"]
            operator.country_of_registration = dict_operator["country_of_registration"]
            operator.accepted_currency = dict_operator["accepted_currency"]
            operator.description = dict_operator["description"]
            operator.average_price = dict_operator["average_price"]
            return operator.save(function (err, res) {
                if (err) return console.error(err)
                return res
              })
        }   
        catch (err) {
            console.log(err)
        }
            

    }

    async delete_tour_operator(id) {
        TourOperatorForMongo.findByIdAndRemove(id, function (err, doc) {
            if (err){
                return err
            }
            return doc
        })

    }
}

module.exports = TourOperatorRepository