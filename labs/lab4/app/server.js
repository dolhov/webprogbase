

// const Router = require('./routers/router.js');
const express = require('express');
const apiRouter = require('./routes/api');
let port = 3001;
const mongoose = require('mongoose');
 
const config = require('./config')

const mustache = require('mustache-express');
const path = require('path');

const app = express();

const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache(path.join(viewsDir, "partials")));


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'mst');

const busboy = require('busboy-body-parser');
app.use(express.static('public'));


const options = {
   limit: '5mb',
   multi: false,
};
app.use(busboy(options));
app.use('/api', apiRouter);
const expressSwaggerGenerator = require('express-swagger-generator');
const expressSwagger = expressSwaggerGenerator(app);
// const expressSwaggerGenerator = require('express-swagger-generator');
// //const { config } = require('process');
// const expressSwagger = expressSwaggerGenerator(app);
 
const optionsSwagger = {
    swaggerDefinition: {
        info: {
            description: 'Server for operating vacations',
            title: 'Vacation Server',
            version: '1.0.0',
        },
        host: `webprogbaselab4.herokuapp.com`,
        produces: [ "application/json" ],
    },
    basedir: __dirname,
    files: ['./routes/**/*.js', './models/**/*.js'],
};
expressSwagger(optionsSwagger);





  

// app.use(function(err, req, res, next) { 
//     console.log(`On error: ${err.message}`); 
// });
// mongodb+srv://axel:666524@cluster0.j4yq0.mongodb.net/
const dbUrl = `mongodb+srv://${config.dev.db.host}${config.dev.db.port}/${config.dev.db.name}`

const connectOptions = {
   useNewUrlParser: true,
   useUnifiedTopology: true,
};

let b = 2

app.listen(config.dev.app.port, async () => { 
    console.log('Server is ready'); 
    const client = await mongoose.connect(dbUrl, connectOptions);
    console.log('Mongo database connected');
});