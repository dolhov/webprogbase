const path = require('path');
const UserRepository = require('./../repositories/user_repository');
const userRepository = new UserRepository("data/users.json");

const Vacation = require('../models/vacation');

const VacationRepository = require('./../repositories/vacation_repository');
const cons = require('consolidate');
const vacationRepository = new VacationRepository("data/vacations.json");

const mediaRepository = require('./../repositories/media_repository');
const vacation = require('../models/vacation');
const media_repository = new mediaRepository();


let b = 2
module.exports = {

    getIndex(req, res) {
        res.render('index', { })
    },

    async getUsers(req, res) {
        let users = await userRepository.getUsers(100, 0)
        // let repo = []
        // users.forEach(element => {
        //     repo.push(userRepository.convertUserToDict(element))
        // });
        res.render('users', {'repo': users})
        // res.sendFile(path.join(__dirname, '../views', 'users.mst'))
    }, 

    async getUser(req, res) {
        let id = req.params.id
        let usr = await userRepository.getUserById(id)
        let dic = userRepository.convertUserToDict(usr)
        if (usr) {
            res.render(`user`, dic)
        }
        else {

        }
        // res.sendFile(path.join(__dirname, '../views', `user${id}.html`))
    }, 

    async getVacations(req, res) {
        let search_name = req.query["tourop_name"]
        let page = parseInt(req.query["page"])
        let result = "Nothing found on that page/search"
        if (!page) {
            page = 1
        }
        console.log("Before length")
        let vacations_overall = await vacationRepository.getVacationsLength(search_name)
        console.log(vacations_overall)
        console.log("after length")
        let vacations = await vacationRepository.getVacations(10 * page, 10 * (page - 1), search_name)
        console.log("After vacations")
        let repo = []
        console.log(vacations)
        vacations.forEach(element => {
            repo.push(vacationRepository.convertVacationToDict(element))
        });
        if (repo.length) {
            result = "Something found"
        }
        let overall_pages = 0
        
        if (vacations_overall != 0) {
            overall_pages = Math.floor(vacations_overall / 10) + 1
        }
        let next_page = page + 1
        let previous_page = page - 1
        if (overall_pages <= page) {
            page = overall_pages
            next_page = page
            previous_page = page - 1
        }
        if (previous_page < 1) {
            previous_page = 1
        }
        res.render('vacations', {"repo": repo,"overall_pages": overall_pages,
                                 "current_page": page, "next_page": next_page,
                                 "previous_page": previous_page, "tourop_name": search_name, 
                                 "result": result })
        //res.sendFile(path.join(__dirname, '../views', 'vacations.html'))
    }, 

    async getVacation(req, res) {
        let id = req.params.id
        let vacation = await vacationRepository.getVacationById(id)
        res.render('vacation', vacation)
        // res.sendFile(path.join(__dirname, '../views', `vacation${id}.html`))
    }, 

    getAbout(req, res) {
        res.render('about', { })
        //res.sendFile(path.join(__dirname, '../views', 'about.html'))
    }, 

    getNew(req, res) {
        res.render('new', { })
    },

    async postVacation(req, res) {
        let tour_operator = req.body["tourop_name"]
        let country = req.body["country"]
        let cost = req.body["cost"]
        let duration = req.body["duration"]
        let date = req.body["date"]
        if (!(tour_operator && country && cost && duration && date)) {
            // send bad code 
            res.status(400).send("Bad request")
            return 
        }
        if (!(cost > 0 && duration > 0)) {
            res.status(400).send("Bad request")
            return 
        }
        let vac_img = req.files['image'].data
        let id = media_repository.postMedia(vac_img)
        let vacation = {"tour_operator": tour_operator, "country": country, "duration": duration, "cost": cost, "date": date, "image": `/api/data/media/${id}`}
        vacation['id'] = await vacationRepository.addVacation(vacation)
        res.render("vacation", vacation)
    },

    getImage(req, res) {
        let id = parseInt(req.params.id)
        res.sendFile(path.join(__dirname, `../data/media/${id}`))
    },

    async deleteVacation(req, res) {
        let id = req.params.id
        let result = await vacationRepository.deleteVacation(id)
        console.log(result)
        res.status(200).send("Ok")
    }
}
