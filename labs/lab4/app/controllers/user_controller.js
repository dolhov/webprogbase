

const UserRepository = require('./../repositories/user_repository');
const MediaRepository = require('./../repositories/media_repository');
 
const userRepository = new UserRepository("data/users.json");
const mediaRepository = new MediaRepository()

module.exports = {
 
    async getUsers(req, res) {
        let limit = parseInt(req.query.limit)
        let page = parseInt(req.query.page)
        if (page < 1) {
            res.status(400).send("Bad request")
        }
        let users = await userRepository.getUsers(limit, (page - 1) * limit);
        let us = [];
        for (i = 0; i < users.length; i++) {
            us += `\"id\": ${users[i]["id"]}, "fullname": ${users[i]["fullname"]}\n`;
        }
        res.send(users);
    },
 
    async getUser(req, res) {
        const userId = req.params.id;
        const user = await userRepository.getUserById(userId);
        if (!user) {
            res.status(404).send(`Not found`);
            return;
        }
        let ans = "";
        for (key in user) {
            ans += key + ": " + user[key] + "\n"
        }
        res.send(user);
    },

    async postUser(req, res) {
        try {
            let today = new Date()
            let ava = req.files["avatar"].data
            let avaUrl = await mediaRepository.postMedia(ava)
            avaUrl = avaUrl["url"]
            // user = req.files['user'].data.toString()
            let user = {"vacations": [], "login": req.query.login, 
                    "fullname": req.query.fullname, "role": req.query.role,
                    "registeredAt": today, "avaUrl": avaUrl, "isEnabled": true}
            result = await userRepository.postUser(JSON.stringify(user))
            await res.status(200).send(JSON.stringify(result))
        }
        catch (err) {
            console.log(err)
            res.status(400).send(err)
        }
    },

    async postVacationUser(req, res) {

        try {
            let user_id = req.params.id
            let vac_id = req.query.vacation_id
            let result = await userRepository.postVacationToUser(user_id, vac_id)
            if (result) {
                return await res.send(result)
            }
            res.status(400).send("No vacation with such id")
        }
        catch (err) {
            console.log(err)
            res.status(400).send(err)
        }
    }
};
