const VacationRepository = require('./../repositories/vacation_repository');
const Vacation = require('../models/vacation');



const vacationRepository = new VacationRepository("data/vacations.json");
 
module.exports = {

    async postVacation(req, res) {
        let vac
        try {
            vac = JSON.parse(req.files['vacation'].data.toString())
            let tourop = vac["tour_operator"]
            // if (tourop.length != 24) {
            //     throw "Incorrect id of touroperator. It shoud consist 24 hex characters "
            // }
            let country = vac["country"]
            let duration = vac["duration"]
            let cost = vac["cost"]
            let date = vac["date"]
            if (!tourop || !country || !duration || !cost || !date) {
                res.status(400).send("Bad request")
                return
            }
        }
        catch (err) {
            res.status(400).send(err)
            
        }
        try {
            let new_id = await vacationRepository.addVacation(vac);
            res.send(await vacationRepository.getVacationById(new_id))
        }
        catch (err) {
            res.status(400).send(err)
        }
    },

    async getVacations(req, res) {
        let limit = parseInt(req.query.limit)
        let page = parseInt(req.query.page)
        if (page < 1) {
            res.status(400).send("Bad request")
        }
        const vacations = await vacationRepository.getVacations(limit, (page - 1) * limit);
        if (vacations) {
            res.send(vacations);
        }
    },

    async getVacation(req, res) {
        const vacation_id = req.params.id
        const vac = await vacationRepository.getVacationById(vacation_id);
        if (!vac) {
            res.status(404).send(`Not found.`);
            return;
        }
        res.send(vac);
    },

    async updateVacation(req, res) {
        let vacation
        
        try {
            vacation = JSON.parse(req.files['vacation'].data.toString())
            let id = vacation["id"]
            let tourop = vacation["tour_operator"]
            let country = vacation["country"]
            let duration = vacation["duration"]
            let cost = vacation["cost"]
            let date = vacation["date"]
            console.log(vacation)
            if (!tourop || !country || !duration || !cost || !date || !id) {
                res.status(400).send("Bad request")
                return
            }
        }
        catch (exception) {
            console.log(exception)
            res.status(400).send("Bad request")
            return
        }
        const vac = await vacationRepository.updateVacation(vacation);
        if (!vac) {
            res.status(404).send("Not found");
            return;
        }
        res.send(vac)
    },


    async deleteVacation(req, res) {
        const vacation_id = req.params.id
        let vc = await vacationRepository.deleteVacation(vacation_id)
        if (!vc)  {
            res.status(404).send("Not found")
        }
        res.send(vc)
    },
};
