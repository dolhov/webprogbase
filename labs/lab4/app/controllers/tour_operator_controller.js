const tourOperatorRepository = require('./../repositories/tour_operator_repository.js');
const tour_operator_repository = new tourOperatorRepository();


module.exports = {
    async post_tour_operator(req, res) {
        try {
            console.log("start")
            tour_operator = JSON.parse(req.files['tour_operator'].data.toString())
            name = tour_operator["name"]
            country = tour_operator["country_of_registration"]
            currency = tour_operator["accepted_currency"]
            description = tour_operator["description"]
            if (name && country && currency && description)
            {
                result = await tour_operator_repository.insert_tour_operator(tour_operator)
                res.send(result)
            }
            else {
                res.status(400).send("Bad request")
            }
            
        }
        catch (err) {
            console.log(err)
            res.status(500).send("Internal server error")
        }
    },
    
    async get_tour_operator(req, res) {
        console.log("start")
        id = req.params.id
        console.log(result)
        tour_operator = await tour_operator_repository.get_tour_operator(id)
        if (!tour_operator) {
            res.status(404).send(`Not found`);
            return;
        }
        res.send(tour_operator) 
    },

    async get_all_tour_operators(req, res) {
        tour_operators = await tour_operator_repository.get_all_tour_operators()
        res.send(tour_operators)
    },

    async update_tour_operator(req, res) {
        tour_operator = req.files['tour_operator'].data.toString()
        result = await tour_operator_repository.update_tour_operator(tour_operator)
        return res.send(result)
    },

    async delete_tour_operator(req, res) {
        id = req.params.id
        let tour_operator = await tour_operator_repository.delete_tour_operator(id)
        if (tour_operator)
        {
            res.send(tour_operator)
        }
        else {
            res.status(404).send("Not found")
        }
    }

}



